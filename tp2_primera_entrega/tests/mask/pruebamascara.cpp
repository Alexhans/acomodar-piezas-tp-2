#include <iostream>
#include <vector>
#include <string>
// #include <cassert>

#include "posicion.h"
#include "transformacion.h"
#include "mascara.h"

void check(bool expr, std::string str="") {
    std::string message = "expression failed";
    if(expr) message = "expression passed";
    std::cout << message << " - " << str << std::endl;
}

int main() {
    using std::cout;
    using std::endl;

    Posicion a(2,3);
    std::cout << "a = " << a << endl;
    Posicion b(1,4);
    std::cout << "b = " << b << endl;
    Posicion c = a;
    std::cout << "c = " << c << endl;

    // mostrar(a,b,"<", a<b);
    cout << a << " < "  << b << ": " << (a < b) << endl;
    cout << a << " > "  << b << ": " << (a > b) << endl;
    cout << a << " <= " << b << ": " << (a <= b) << endl;
    cout << a << " >= " << b << ": " << (a >= b) << endl;
    cout << a << " == " << b << ": " << (a == b) << endl;
    cout << a << " != " << b << ": " << (a != b) << endl;
    cout << a << " == " << c << ": " << (a == c) << endl;
    cout << a << " != " << c << ": " << (a != c) << endl;


    CatalogoUnicoMascaras catalogo;
    std::string archivoMascaras = "mascaras_de_prueba.txt";
    // catalogo = cargarCatalogo(archivoMascaras);
    cargarCatalogo(&catalogo, archivoMascaras);
    check(false == checkisColisionan(catalogo['A'], catalogo['B'], Posicion(1,0)), "cuadrado y L con cuadrado corrido 1");
    check(true == checkisColisionan(catalogo['A'], catalogo['B']), "cuadrado y L");
	Transformacion abajo(Transformacion::ABAJO);
	Mascara bTransformada = abajo.transformar(catalogo['B']);
    check(false == checkisColisionan(bTransformada, catalogo['B'], Posicion(0,0), Posicion(0,1)), "L y L(0,1) para abajo" );
    check(true == checkisColisionan(catalogo['B'], catalogo['C']), "L y L invertida");
    check(false == checkisColisionan(catalogo['H'], catalogo['F']), "simple punto y podio");
    check(false == checkisColisionan(catalogo['D'], catalogo['F'], Posicion(0,0), Posicion(0,2)), "s y podio corrido");
    check(true == checkisColisionan(catalogo['D'], catalogo['F'], Posicion(1,0), Posicion(1,1)), "s y podio corrido (COLISIONA)");

    //  Esto no debe ser parte de las pruebas, hay que describir a numeros negativas en posicion como undefined behaviour.
    // check(true == checkisColisionan(catalogo['I'],catalogo['J'],Posicion (0,0), Posicion(-1,0)),
    //                                  "Dos podios diferentes, uno en el (0,0) y el otro en el (-1,0)");

    // check(false == checkisColisionan(catalogo['I'],catalogo['J'],Posicion (0,0), Posicion(-1,0), Transformacion(Transformacion::ESPEJO_HORIZONTAL)),
    //                                  "Dos podios diferentes, uno en el (0,0) y el otro en el (-1,0), el segundo esta rotado hacia la derecha");

    check(false == checkisColisionan(catalogo['K'],catalogo['L'],Posicion (0,0), Posicion(1,0)),
                                     "Una ficha rara con un hueco en la posicion (1,0) y (2,0), y una ficha de 2x1. La primera esta en el(0,0) y el otro en el (1,0)");

	Transformacion espejada_vert(Transformacion::ESPEJO_VERTICAL);
	Mascara iTransformada = espejada_vert.transformar(catalogo['I']);
    check(false == checkisColisionan(iTransformada,catalogo['J'],Posicion (0,0), Posicion(1,0)),
                                     "Una ficha rara con un hueco en la posicion (1,0) y (2,0), y una ficha de 2x1. La primera esta en el(0,0) y el otro en el (1,0), probe con todos los tipos de rotaciones y funiono");

    for(CatalogoUnicoMascaras::iterator it=catalogo.begin();
        it!=catalogo.end();
        ++it)
    {
		std::cout << it->first << std::endl << std::endl;
        it->second.show(false);
        std::cout << std::endl;
    }
    // Mascara test;
    // test.show();
}

