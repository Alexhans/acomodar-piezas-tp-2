#include <iostream>
#include "posicion.h"
#include "transformacion.h"
#include "mascara.h"


void verificarTransformacion(std::string descripcion,
                             Mascara original,
                             Mascara esperada,
                             Transformacion trans) {
    std::cout << descripcion << std::endl << std::endl;
    Mascara resultante = trans.transformar(original);

    std::cout << "Resultante:" << std::endl;
    resultante.show(false);
    std::cout << std::endl;

    std::cout << "Esperada:" << std::endl;
    esperada.show(false);
    std::cout << std::endl;

    std::string mensaje = (resultante == esperada) ? "SUCCESS" : "**FAILURE**";
    std::cout << "verifica: " << mensaje << std::endl;  
    std::cout << std::endl;

}

int main(int argc, char **argv) {
    using namespace std;

    std::string archivoMascaras = "esperado.txt";
    if(argc > 1) {
        archivoMascaras = argv[1];
    }

    CatalogoUnicoMascaras catalogo;
    cargarCatalogo(&catalogo, archivoMascaras);

    Mascara& sinRotacion = catalogo['A'];
    Mascara& rotadaDer   = catalogo['B'];
    Mascara& rotada180   = catalogo['C'];
    Mascara& rotadaIzq   = catalogo['D'];
    Mascara& espejadaVer = catalogo['E'];
    Mascara& espejadaHor = catalogo['F'];

    Transformacion transSinRotacion(Transformacion::ARRIBA);
    Transformacion transAbajo(Transformacion::ABAJO);
    Transformacion transDerecha(Transformacion::DERECHA);
    Transformacion transIzquiera(Transformacion::IZQUIERDA);
    Transformacion transEspHor(Transformacion::ESPEJO_HORIZONTAL);
    Transformacion transEspVer(Transformacion::ESPEJO_VERTICAL);

    cout << "Original" << endl << endl;
    sinRotacion.show(false);
    cout << endl;

    verificarTransformacion("sinRotacion", sinRotacion, sinRotacion, transSinRotacion);
    verificarTransformacion("rotada Derecha", sinRotacion, rotadaDer, transDerecha);
    verificarTransformacion("rotada Izquierda", sinRotacion, rotadaIzq, transIzquiera);
    verificarTransformacion("rotada Abajo", sinRotacion, rotada180, transAbajo);
    verificarTransformacion("espejada Vertical", sinRotacion, espejadaVer, transEspVer);
    verificarTransformacion("espejada Horizonal", sinRotacion, espejadaHor, transEspHor);
    
}
