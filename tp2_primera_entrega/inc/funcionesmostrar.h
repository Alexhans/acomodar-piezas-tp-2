#include <iostream>
#include <string>
#include "datosgrilla.h"
#include "representaciongrilla.h"
#include "resoluciongrilla.h"

     /* Pos: muestra por pantalla las dimensiones de la grilla.
     */

void mostrarDimensiones(const DatosGrilla &grilla);

     /* Pos: muestra por pantalla la secuencia y cuantas fichas a colocar contiene la misma.
     */

void mostrarSecuencia(const Secuencia &secuencia);

     /* Pos: muestra por pantalla la ocupacion de la grilla, la cantidad de piezas colocadas y la cantidad
     * de piezas que no pudieron ser colocadas.
     */

void mostrarInfoSolucion(const ResolucionGrilla& solucion);

