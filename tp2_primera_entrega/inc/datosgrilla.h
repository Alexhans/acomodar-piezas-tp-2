#ifndef H_GRILLA
#define H_GRILLA
#include <string>
#include "mascara.h"

// TODO: ubicarlo en el namespace adecuado.
// TODO: Constructor de copia? asignacion?
// destructor vacio por que los elementos de STL se liberan solos.
// TODO: guardar las rutas en un string?

class DatosGrilla {
public:
    /* Pre: ruta a Archivo válido de catalogo.  IDEM secuencia.
     * Pos: datos cargados en sus respectivas listas.
     */
    DatosGrilla(std::string catalogo, std::string secuencia);
    /* Pre: ruta a Archivo válido de catalogo.
     * Pos:
     */
    void cargarSecuencia(const std::string &ruta);
    /* Pre: ruta a Archivo válido de secuencia.
     *
     * Pos:
     */
    void cargarCatalogo(const std::string &ruta);


    // Getters
    int getAlto() const;
    int getAncho() const;

    Secuencia getSecuencia() const;

    CatalogoUnicoMascaras getCatalogo() const;

private:
    int alto;
    int ancho;
    Secuencia secuencia;
    CatalogoUnicoMascaras catalogo;
};

#endif
