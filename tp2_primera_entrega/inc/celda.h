#ifndef TABLERO_H_INCLUDED
#define TABLERO_H_INCLUDED

#include "resoluciongrilla.h"
#include "EasyBMP.h"


class Celda{
public:
    enum Lado {ARRIBA, DERECHA, ABAJO, IZQUIERDA};

        /* Pos: Crea una instancia de una celda, incialmente vac�a, sin bordes y de color blanco.
        */
        Celda();

        /* Pos: Celda ocupada o desocupada no seg�n el par�mentro.
        */
        void ocupar(bool estaOcupada);

        /* Pos: Cambiado el color de la celda.
        */
        void setColor(RGBApixel color);

        /* Pos: Se cambia la existencia de bordes de una celda.
        */
        void setBorde(Lado lado, bool esBorde);

        /* Pos: Devuelve true si la celda esta ocupada; false si esta vacia.
        */
        bool ocupada() const;

        /* Pos: Devuelve el color de la celda.
        */
        RGBApixel getColor() const;

        /* Pre: Celda (i,j) v�lida, o sea, contenida en las dimensiones de la grilla.
        * Pos: Queda dibujada la celda (cuadrada, de lado 'tamanioCelda' pixels), con su respectivo color y
        * sus bordes (de exisistir), en el BMP pasado por parametro.
        */
        void graficarCelda(int i, int j, BMP& grafico, int tamanioCelda);


    private:
        RGBApixel color;
        bool bordes[4];     // VERDADERO/FALSO seg�n si hay que dibujar bordes en cada lado (0:arriba, 1:der, 2:abajo, 3:izq)
        bool estaOcupada;

        /* Pos: Devuelve el color con el que se debe dibujar el borde: Negro si existe borde,
        * el color del interior de la celda si no existe.
        */
        RGBApixel colorBorde(Lado lado) const;
};


#endif // TABLERO_H_INCLUDED
