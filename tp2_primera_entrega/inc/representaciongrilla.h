#ifndef H_REPRESENTACION_GRILLA
#define H_REPRESENTACION_GRILLA
#include <string>
#include <map>
#include "EasyBMP.h"
#include "resoluciongrilla.h"
#include "mascara.h"
#include "celda.h"

//TODO: decidir si hacerlos o ponerlos en privado.
//Constructor de copia
//Constructor de asignación.

/*
	Se encarga de, en base a una solución, representarla de alguna manera.

	exportarComoImagen, por ejemplo, es un metodo que la representa escribiendola
	en un archivo de formato BMP.
*/
class RepresentacionGrilla {

public:

    /* Pos: construye la grilla donde se va a colocar la solucion del problema y
    * crea la paleta colores.
    */
    RepresentacionGrilla(const ResolucionGrilla& resolucion);

    ~RepresentacionGrilla();

    /*
    * Pos: Dibuja las piezas y la grilla segun la resolucion, en una imagen y la
    * escribe en un archivo.
    */
    void exportarComoImagen(std::string ruta);

private:
    void dibujarPieza(PiezaIngresada pieza);
    BMP representacion;
    int x;  //cantidad de pixels de ancho del .bmp
    int y;  //cantidad de pixels de alto del .bmp
    std::map <IdElementoSecuencia, RGBApixel> paletaColores;
    ResolucionGrilla resolucion;
};

#endif
