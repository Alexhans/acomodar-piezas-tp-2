#ifndef H_RESOLUCION_GRILLA
#define H_RESOLUCION_GRILLA
#include <vector>
#include "datosgrilla.h"
#include "transformacion.h"

// TODO:  Analizar no pasar la copia de mascara (y pasar catalogo en ResolucionGrilla)
// ver doc/optimizaciones.txt

// estructura que guarda la informaci�n relevante cada vez que se ingresa una pieza en un metodo
// de soluci�n.

struct PiezaIngresada {
    PiezaIngresada(
            const IdElementoSecuencia&,
            const Mascara &,
            const Posicion &,
            const Transformacion &);
    Mascara mascara;
    Posicion posicion;
    Transformacion transformacion;
    IdElementoSecuencia tipoPieza;
};

///////////////////////////////////////////////////////////////////////////////
//  RESOLUCIONGRILLA
///////////////////////////////////////////////////////////////////////////////

/*
	Representa la soluci�n generada en base a datos (una secuencia, un catalogo y dimensiones).
	En general, es construida por funciones no miembro de resolucion.

	Ejemplo: resolverAlgoritmoPanch o resolverAlgoritmoTipoTP1.

	Posee la informacion de todas las piezas ingresadas, restantes y tambi�n una representacion
	de mascara interna.
*/
class ResolucionGrilla {

public:

    typedef std::vector<PiezaIngresada> ListaPiezasIngresadas;
    typedef std::vector<Mascara> ListaMascaras;

    ResolucionGrilla();

    /* Pos: devuelve la cantidad de piezas utilizadas, que fueron colocadas en la grilla.
    */
    int getCantidadPiezasUsadas() const;

    /* Pos: devuelve la cantidad de piezas que no fueron colocadas en la grilla.
    */
    int getCantidadPiezasSinUsar() const;

    /* Pos: devuelve la cantidad de lugares ocupados en toda la grilla.
    */
    int getLugaresOcupados() const;

    /* Pos: devuelve la superficie total de la grilla(es la multiplicacion del ancho por el alto)
    */
    int getDimension() const;

    /* Pos: crea una lista conteniendo todas las piezas que fueron colocadas en la grilla.
    */
    ListaPiezasIngresadas& getPiezasIngresadas();

    /* Pos: crea una lista conteniendo todas las piezas que no fueron colocadas en la grilla.
    */
    ListaMascaras& getPiezasRestantes();

    /* Pos: Setea en la grilla toda la resolucion, con los valores verdaderos o falsos dependiendo de si
    * el casillero se encuentra ocupado o no.
    */
    void setMascaraFinal(Mascara &masc);

    /* Pos: Devuelve la grilla conteniendo la resolucion final.
    */
    Mascara getMascaraFinal() const;

private:

    Mascara mascaraFinal;
    ListaPiezasIngresadas piezasIngresadas;
    ListaMascaras piezasRestantes;
};

///////////////////////////////////////////////////////////////////////////////
//  NON MEMBER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////


/* Pos:  Resuelve el problema con el algortimo del tp2 sin hacer transformacion.
*/
ResolucionGrilla resolverAlgoritmoPanchSinTransformaciones(const DatosGrilla &datosGrilla);  // metodo?.

/* Pos: Resuelve el problema con el algortimo del tp1.
*/
ResolucionGrilla resolverAlgoritmoTipoTP1(const DatosGrilla &datosGrilla);

/* Pos: Resuelve el problema con el algortimo del tp2 haciendo las transformaciones.
*/
ResolucionGrilla resolverAlgoritmoPanch(const DatosGrilla &datosGrilla);

#endif
