#include <iostream>
#include <string>

#include "datosgrilla.h"
#include "representaciongrilla.h"
#include "resoluciongrilla.h"
#include "funcionesmostrar.h"

int main(int argc, char **argv) {

    /////////////////////////////////
    // CARGAR DATOS
    /////////////////////////////////

	// rutas por defecto

    std::string archivoCatalogoPorDefecto = "data/catalogo/default.txt";
    std::string archivoSecuencia = "data/secuencias/a.txt";

    // para probar con otros archivos:
    // ej:  acomodar.exe data/secuencias/b.txt data/catalogo/default.txt

    if(argc > 1) {
        archivoSecuencia = argv[1];
        if(argc > 2) {
            archivoCatalogoPorDefecto = argv[2];
        }
    }

    std::cout << "Bienvenido a la resolucion del Trabajo Practico 2 de la \ncatedra Calvo de Algoritmos y Programacion 2." << std::endl << std::endl;
    std::cout << "Los integrantes del grupo son: Gaston Gofreddo, Alex Guglielmone Nemi, \nTobias Lichtig, Santiago Acu�a y Francisco Landino." << std::endl << std::endl;
    std::cout << "Grupo 8, 1er Cuatrimestre 2013. Corrector: Gustavo Schmidt." << std::endl << std::endl;

	std::cout << "------------------------------------------------------------" << std::endl << std::endl;

    std::cout << "Cargando catalogo de piezas desde: " << archivoCatalogoPorDefecto << std::endl << std::endl;
    std::cout << "Cargando secuencia de piezas desde: " << archivoSecuencia << std:: endl << std:: endl;

    DatosGrilla datos(archivoCatalogoPorDefecto, archivoSecuencia);

	/////////////////////////////////
    // MOSTRAR INFO DATOS
    /////////////////////////////////

    mostrarDimensiones(datos);

    mostrarSecuencia(datos.getSecuencia());

    /////////////////////////////////
    // RESOLVER
    /////////////////////////////////

    std::cout << std::endl << "Resolviendo con algortimo del TP2, por favor espere..." << std::endl << std::endl;

    ResolucionGrilla solucion = resolverAlgoritmoPanch(datos);

	mostrarInfoSolucion(solucion);

    /////////////////////////////////
    // REPRESENTAR
    /////////////////////////////////

    std::string rutaImagen = "resolucion.bmp";

    RepresentacionGrilla representacion(solucion);

    representacion.exportarComoImagen(rutaImagen);

    std::cout << std::endl << "La imagen donde se encuentra la solucion esta en la misma carpeta que el ejecutable con el nombre de: " << rutaImagen << std::endl;

    /////////////////////////////////
    // RESOLVER Y MOSTRAR DATOS TP1
    /////////////////////////////////

	std::cout << std::endl << "Resolviendo con algoritmo TP1, por favor espere..." << std::endl;

    ResolucionGrilla solucionTP1 = resolverAlgoritmoTipoTP1(datos);
	mostrarInfoSolucion(solucionTP1);

}





