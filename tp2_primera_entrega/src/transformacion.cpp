///////////////////////////////////////////////////////////////////////////////
//  TRANSFORMACION
///////////////////////////////////////////////////////////////////////////////

#include "transformacion.h"

Transformacion::Transformacion(Tipo trans): transformacion(trans) {}

Transformacion::Tipo Transformacion::getTipo() const {return transformacion;}

void Transformacion::setTipo(Transformacion::Tipo trans) { transformacion = trans; }

Mascara Transformacion::transformar(const Mascara &original) {
    // por ahora, se puede hacer con un switch.
    // Aca me fijo con que nuevas dimensiones creo la nueva mascara, si es izquierda o derecha, las coordenadas se intercambian, sino
    // son iguales
    switch(this->getTipo()){
    case ARRIBA:
        return original;
        break;
    case ABAJO:
        return this->rotarAbajo(original);
        break;
    case DERECHA:
        return this->rotarDerecha(original);
        break;
    case IZQUIERDA:
        return this->rotarIzquierda(original);
        break;
    case ESPEJO_HORIZONTAL:
        return this->espejarHorizontal(original);
        break;
    case ESPEJO_VERTICAL:
        return this->espejarVertical(original);
        break;
    }
    return original;
}

Mascara Transformacion::espejarVertical(const Mascara &original){
    Mascara mascaraFinal = Mascara(original.getAncho(), original.getAlto());
    for (int y = 0;y < original.getAlto(); y++){
        for(int x = 0; x < original.getAncho();x++){
            bool valor = original.getValor(x,y);
            int nuevoX = x;
            int nuevoY = original.getAlto() - (y + 1);
            mascaraFinal.setValor(nuevoX, nuevoY, valor);
        }
    }
    return mascaraFinal;

}


Mascara Transformacion::espejarHorizontal(const Mascara &original){
    Mascara mascaraFinal = Mascara(original.getAncho(), original.getAlto());
    for (int y = 0;y < original.getAlto();y++){
        for(int x = 0; x < original.getAncho(); x++){
            bool valor = original.getValor(x, y);
            int nuevoX = original.getAncho() - (x + 1);
            int nuevoY = y;
            mascaraFinal.setValor(nuevoX, nuevoY, valor);
        }
    }
    return mascaraFinal;

}

Mascara Transformacion::rotarDerecha(const Mascara &original){
     Mascara mascaraFinal = Mascara(original.getAlto(), original.getAncho());
             for(int y = 0;y < original.getAlto();y++){
                for(int x = 0;x < original.getAncho();x++){
                    bool valor = original.getValor(x,y);
                    int nuevoX =original.getAlto() - y - 1 ;
                    int nuevoY =x;
                    mascaraFinal.setValor(nuevoX, nuevoY, valor);
                }
             }
    return mascaraFinal;
}

Mascara Transformacion::rotarIzquierda(const Mascara &original){
     Mascara mascaraFinal = Mascara(original.getAlto(), original.getAncho());
             for(int y = 0;y < original.getAlto();y++){
                for(int x = 0;x < original.getAncho();x++){
                    bool valor = original.getValor(x,y);
                    int nuevoX =y;
                    int nuevoY =original.getAncho() - x - 1;
                    mascaraFinal.setValor(nuevoX, nuevoY, valor);
                }
             }
    return mascaraFinal;
}

Mascara Transformacion::rotarAbajo(const Mascara &original){
     Mascara mascaraFinal = Mascara(original.getAncho(), original.getAlto());
             for(int y = 0;y < original.getAlto();y++){
                for(int x = 0;x < original.getAncho();x++){
                    bool valor = original.getValor(x,y);
                    int nuevoX =original.getAncho() - x - 1 ;
                    int nuevoY =original.getAlto() - y - 1;
                    mascaraFinal.setValor(nuevoX, nuevoY, valor);
                }
             }
    return mascaraFinal;
}
