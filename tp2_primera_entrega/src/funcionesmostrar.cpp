#include "funcionesmostrar.h"



void mostrarSecuencia(const Secuencia &secuencia) {
    std::cout << "Secuencia: " << secuencia.size();
    for(unsigned int i=0; i<secuencia.size(); ++i) {
        if(i % 79 == 0) std::cout << std::endl;  // cortar cada 79 pora que se lea bien en una terminal comun (aprox 80)
        std::cout << secuencia[i];

    }
    std::cout << std::endl;

}

void mostrarDimensiones(const DatosGrilla &grilla) {
    std::cout << "Dimensiones: (" << grilla.getAlto() << ", " << grilla.getAncho() << ")" << std::endl;
}

void mostrarInfoSolucion(const ResolucionGrilla& solucion) {
    std::cout << std::endl << std::endl;
    std::cout << "Posicion final con algoritmo" << std::endl;

    int ocupacion = solucion.getLugaresOcupados();
    int dimension = solucion.getDimension();
    double factor_de_ocupacion = ocupacion / static_cast<double>(dimension);
    std::cout << "factor: " << factor_de_ocupacion << " - " << factor_de_ocupacion*100 << " % ";
    std::cout << "piezas utilizadas: " << solucion.getCantidadPiezasUsadas() << std::endl;
    std::cout << "piezas sin usar: " << solucion.getCantidadPiezasSinUsar() << std::endl << std::endl;
}
