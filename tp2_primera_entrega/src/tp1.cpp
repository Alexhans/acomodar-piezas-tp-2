#include <iostream>
#include "resoluciongrilla.h"

void mostrarInfoSolucion(const ResolucionGrilla& solucion);

// Devuelve el 1 mas a la izquierda
// si ninguna es, devuelve 0.
int primerPosicionOcupadaDerecha(const Mascara& mascara) {
    unsigned int ancho = mascara.getAncho();
    for(unsigned int i = ancho - 1; i>=0; --i) {
        if(mascara.getValor(i,0)) {
            return i;
        }
    }
    return 0;
}

/*
 * 1 - Recorre cada pieza insertandolas en determinada posicion hasta que
 * no entren ni de ancho ni de alto.
 *
 * 2 - El criterio de insercion tiene que ver con dos cosas.
 *
 *  Una:  Si es posible, usar como ancla para intentar insertar, la siguiente posicion
 *  a la ultima ocupada de la linea superior.
 *
 *  Dos:  Si no entra en lo que queda de la grilla, bajar hasta un casillero
 *   despues de la base de la pieza ubicada mas abajo.
 *
 *    0123456
 *
 *  0 xx x
 *  1  xxxx
 *  2
 *
 *  Intentaria usar como ancla (x, y) = (4, 0)
 *  Si no pudiese entrar de ancho, usaria (x, y) = (0, 2)
 *
 *  3 - Si no puede usar ninguno de los dos criterios, deja de intentar meter piezas.
 */
ResolucionGrilla resolverAlgoritmoTipoTP1(const DatosGrilla &datosGrilla) {
    ResolucionGrilla solucion;

    std::vector<IdElementoSecuencia> secuencia = datosGrilla.getSecuencia();

    // Acá se guardaran las piezas a medida que satisfagan una u otra condicion
    ResolucionGrilla::ListaPiezasIngresadas piezasIngresadas;
    ResolucionGrilla::ListaMascaras piezasSobrantes;

    CatalogoUnicoMascaras catalogo = datosGrilla.getCatalogo();

    const Transformacion arriba(Transformacion::ARRIBA); //EL TP1 NO ROTA PIEZAS

    const int ANCHO_GRILLA = datosGrilla.getAncho();
    const int ALTO_GRILLA  = datosGrilla.getAlto();

    Mascara grillaFinal(ANCHO_GRILLA, ALTO_GRILLA);

    unsigned int i = 0;  //posicion de la pieza en el vector secuencia
    bool entra_de_alto = true;
    int alturaFila = 0; //altura maxima de las piezas en una fila (para hacer los saltos de linea).
    int x = 0; //proxima posicion en x en la grilla final donde probar insertar la siguiente pieza.
    int y = 0; //altura a la que se probara insertar la proxima pieza.

    while (entra_de_alto &&      // entra de alto (si no entra dejar de ingresar)
            i < secuencia.size()    // quedan piezas por poner
            )
    {
        bool isPiezaIngresada = false;

        Mascara mascaraAIngresar(catalogo[secuencia.at(i)]);
        IdElementoSecuencia tipo = secuencia.at(i);

        const int ALTO_PIEZA = mascaraAIngresar.getAlto();
        const int ANCHO_PIEZA = mascaraAIngresar.getAncho();

        entra_de_alto = (y + ALTO_PIEZA <= ALTO_GRILLA);
        bool entra_de_ancho = (x + ANCHO_PIEZA <= ANCHO_GRILLA);

        while (!isPiezaIngresada && // no hay ingresado pieza
               entra_de_alto &&     // entra de alto
               entra_de_ancho)      // entra de ancho
        {
            Posicion posicionAIngresar(x,y);

            bool colisionan = checkisColisionan(mascaraAIngresar, grillaFinal, posicionAIngresar); //TODO: chequear si funciona si una no entra en la otra???

            if (!colisionan){

                grillaFinal.superponer(mascaraAIngresar, x, y); //inserto la pieza en la grilla en la posicion (x,y)

                isPiezaIngresada = true;  // esto quiere decir que saldra del loop

                PiezaIngresada pieza(tipo, mascaraAIngresar, posicionAIngresar, arriba);
                solucion.getPiezasIngresadas().push_back(pieza);

                int alturaTemporal = y + ALTO_PIEZA - 1;
                if (alturaTemporal > alturaFila){
                    alturaFila = alturaTemporal;
                }

                // corre todo el cuadrado de la pieza
                x += primerPosicionOcupadaDerecha(mascaraAIngresar);  // TODO: cambiar a el max de la linea 0 .
            }
            // si colisiona mover a la derecha o a la linea adecuada de abajo.
            else {
                x++;
                entra_de_ancho = (x + ANCHO_PIEZA <= ANCHO_GRILLA);
                if(!entra_de_ancho) {
                    x = 0;
                    y = alturaFila + 1;
                }

            }

            entra_de_alto = (y + ALTO_PIEZA <= ALTO_GRILLA);
        }
        // Si no entraba de ancho.  bajar una fila.
        if(!entra_de_ancho) {
                    x = 0;
                    y = alturaFila + 1;
                }
        else {
            i++;
        }
    }
    if(!entra_de_alto)  // Si ya no entran piezas.
        for (i=i-1 ; i<secuencia.size(); i++){   //todas las piezas siguientes a la ultima ingresada son piezas restantes
            solucion.getPiezasRestantes().push_back(catalogo[secuencia.at(i)]);
        }
    solucion.setMascaraFinal(grillaFinal);
    return solucion;
}



