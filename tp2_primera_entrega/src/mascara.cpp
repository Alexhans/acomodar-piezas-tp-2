#include "mascara.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib> // para exit

#include "posicion.h"


#define DEBUG_CHECK_COLISION
#undef DEBUG_CHECK_COLISION

///////////////////////////////////////////////////////////////////////////////
//  MASCARA
///////////////////////////////////////////////////////////////////////////////

Mascara::Mascara(unsigned int ancho, unsigned int alto) {
    data.resize(alto);
    for(unsigned int i=0; i<alto; ++i) {
        data.at(i).resize(ancho, false);
    }
}

Mascara::Mascara(const MatrizDeBitsDinamica &matrix):data(matrix) { }

Mascara::Mascara(const Mascara &that):data(that.data) {}

Mascara& Mascara::operator=(const Mascara& that) {
    data = that.data;
    return *this;
}

Mascara::~Mascara() {
    // los contenedores de STL se liberan por si mismos cuando salen de scope.
}

bool operator==(const Mascara& lhs, const Mascara& rhs) {
    int alto = lhs.getAlto();
    int ancho = lhs.getAncho();

    // si las dimensiones son diferentes no pueden ser iguales
    if(alto != rhs.getAlto() || ancho != rhs.getAncho()) {
        return false;
    }

    // Recorre todos los valores de ambos, si encuentra que en algun punto
    // los casilleros no coinciden.  No son iguales.
    bool sonIguales = true;
    for(int x=0; x<ancho; ++x) {
        for(int y=0; y<alto; ++y) {
            if(rhs.getValor(x,y) != lhs.getValor(x,y)) {
                sonIguales = false;
                break;
            }
        }
    }
    return sonIguales;
}

bool operator!= (const Mascara& lhs, const Mascara& rhs) {
    return !operator==(lhs, rhs);
}

void Mascara::superponer(const Mascara& that, unsigned int offsetX, unsigned int offsetY) {
    // TODO comentar como funciona.
    int alto = this->getAlto();
    int ancho = this->getAncho();
    for(int y = offsetY; y < alto; ++y) {
        int superpuestoY = y - offsetY;
        if(superpuestoY >= that.getAlto()) break;
        for(int x = offsetX; x < ancho; ++x) {
            int superpuestoX = x - offsetX;
            if(superpuestoX >= that.getAncho()) break;
            bool nuevoValor = that.getValor(superpuestoX, superpuestoY);
            // agregar el valor si es true.
            if(nuevoValor) this->setValor(x ,y, nuevoValor);
        }
    }
}

void Mascara::show(bool coordenadas) const{
    int count = 0;
    if(coordenadas) {
        std::cout << "  " << "\t" << "01234567890" << std::endl << std::endl;
    }
    for(MatrizDeBitsDinamica::const_iterator i = data.begin();
        i != data.end();
        ++i)
    {
        if(coordenadas) {std::cout << count << "\t"; }
        for(SetDeBitsDinamico::const_iterator j = i->begin();
        j != i->end();
        ++j)
        {
            std::cout << (*j);
        }
        count++;
        std::cout << std::endl;
    }
}

int Mascara::getAlto() const { return data.size(); }

int Mascara::getAncho() const { if(data.size() > 0) return data.at(0).size();
return 0;}

// Atencion: No cambiar el orden a x,y.  Es y,x es correcto por como esta
// armada internamente la matriz.
bool Mascara::getValor(const int x, const int y) const {
    return data.at(y).at(x);
}

// Atencion: No cambiar el orden a x,y.  Es y,x es correcto por como esta
// armada internamente la matriz.
void Mascara::setValor(const int x, const int y, bool valor) {
    data.at(y).at(x) = valor;
}

SetDeBitsDinamico& Mascara::operator[] (const int indice) {
    return data.at(indice);
}

///////////////////////////////////////////////////////////////////////////////
//  SOURCE ONLY HELPER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////

// pos: ordenado por (x,y)  (0,0) (0,1) (1,0) (1,1)
std::vector<Posicion> generarPuntosRectanguloContenedor(const Posicion &offset, const Posicion &dimension) {
    std::vector<Posicion> puntos;
    int ancho = dimension.getX();
    int alto = dimension.getY();
    int offset_x = offset.getX();
    int offset_y = offset.getY();
    for(int y = 0; y < alto; ++y)
        for(int x = 0; x < ancho; ++x)
            puntos.push_back(Posicion(x + offset_x, y + offset_y));
    return puntos;
}

void debugMostrar(const std::vector<Posicion> pos) {
        for(std::vector<Posicion>::const_iterator it = pos.begin();
                it!=pos.end(); ++it) {
            std::cout << (*it) << std::endl;
        }
}


///////////////////////////////////////////////////////////////////////////////
//  NON MEMBER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////

/*
 * 1- Encuentra los puntos de posible conflicto (interseccion de rectangulos contenedores) teniendo en
 * cuenta el desfazaje de posición.
 * 2- Recorre los puntos de conflicto y compara las casillas teniendo en cuenta su desfazaje respectivo.
 * 3- En cuanto encuentra dos casillas ocupadas en cualquier lugar : TRUE
 * 4- Si recorre todos los puntos de conflicto sin encontrar:  FALSE
 */
bool checkisColisionan(const Mascara &lhs,
                            const Mascara &rhs,
                            const Posicion &posLeft,
                            const Posicion &posRight)
    {
        // transformar

        int leftAncho = lhs.getAncho();
        int leftAlto = lhs.getAlto();

        int rightAncho = rhs.getAncho();
        int rightAlto = rhs.getAlto();

        // encontrar los puntos de conflicto.
        // genera el conjunto de puntos de lhs y rhs(ordenado)
        std::vector<Posicion> lhsPuntos = generarPuntosRectanguloContenedor(posLeft, Posicion(leftAncho, leftAlto));

    #ifdef DEBUG_CHECK_COLISION
        std::cout << "Puntos en lhsPuntos\n";
        debugMostrar(lhsPuntos);
    #endif

        std::vector<Posicion> rhsPuntos = generarPuntosRectanguloContenedor(posRight, Posicion(rightAncho, rightAlto));

    #ifdef DEBUG_CHECK_COLISION
        std::cout << "Puntos en rhsPuntos\n";
        debugMostrar(rhsPuntos);
    #endif

        // la union de esos puntos es el conflicto.
        std::vector<Posicion>::iterator it;
        // crea el vector de puntos y reservar espacio para
        // la minima cantidad (que es la max cantidad posible de intersecciones)
        int minimo = std::min(rhsPuntos.size(), lhsPuntos.size());
        std::vector<Posicion> puntosConflicto(minimo); // inicializa puntos invalidos (-1, -1)

        // TODO:  Atencion:  Se supone que genere ordenado.
        // Plantearse si es necesario checkearlo tipo std::is_sorted (c++11, ya se :P)

        // Aca efectivamente calcula los puntos de conflicto como una interseccion.
        it=std::set_intersection(lhsPuntos.begin(), lhsPuntos.end(), rhsPuntos.begin(), rhsPuntos.end(), puntosConflicto.begin());
        puntosConflicto.resize(it-puntosConflicto.begin());

    #ifdef DEBUG_CHECK_COLISION
        std::cout << "Puntos en conflicto\n";
        debugMostrar(puntosConflicto);

        lhs.show();
        rhs.show();
    #endif

        // recorrerlos comparando con los respectivos offset
        for(std::vector<Posicion>::const_iterator i=puntosConflicto.begin(); i!=puntosConflicto.end(); ++i) {
            Posicion lhsPos = (*i) - posLeft;
            Posicion rhsPos = (*i) - posRight;

            bool leftIsOcupado = lhs.getValor(lhsPos.getX(), lhsPos.getY());
            bool rightIsOcupado = rhs.getValor(rhsPos.getX(), rhsPos.getY());

    #ifdef DEBUG_CHECK_COLISION
            std::cout << "compara " << lhsPos << " con " << rhsPos << std::endl;
            std::cout << "left: " << leftIsOcupado << " Right: " << rightIsOcupado << std::endl;
    #endif

            if( leftIsOcupado && rightIsOcupado) {
                return true;
            }
        }
        return false;
    }

//obtenida de : http://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf/6089413#6089413
std::istream& safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}


void cargarCatalogo(CatalogoUnicoMascaras *catalogo, std::string archivo) {
    catalogo->clear();

    std::ifstream file(archivo.c_str(), std::ifstream::in);
    if(!file) { std::cerr << archivo.c_str() << " no pudo ser abierto" << std::endl; exit(1); }

    /*
    while not eof:
        read number of lines
        while not eof and count < lines:
            load non space as 1 and space as 0
            push_back
            verify current width
    */
    // TODO: read everything once
    char identificadorElemento = 'A';
    while(!file.eof()) {

        int lines = 0;
        file >> lines; // TODO check existence

        // IMPORTANT
        MatrizDeBitsDinamica matrix;
        matrix.resize(lines);

        int counter = 0;
        std::string charline = "";
        safeGetline(file, charline); // automaticamente maneja las diferencias CRLF y LF (win/*nix).

        while((counter < lines) && (true)) {

            safeGetline(file, charline);
            // IMPORTANT
            for(std::string::iterator it=charline.begin();
                it!=charline.end();
                ++it) {
                    bool value = (*it)!=' '; // spaces are false
                    matrix.at(counter).push_back(value);
                }

            counter++;
        }

        completarRectangulo(matrix);

        // Agregarlo al mapa con el identificador apropiado
        catalogo->insert( std::pair<IdElementoSecuencia, Mascara>(identificadorElemento, Mascara(matrix)) );
        identificadorElemento++;

    }
    file.close();
}


void completarRectangulo(MatrizDeBitsDinamica &matrix) {

    int max = 0;

    // fijarse cual es la fila mas grande
    for(MatrizDeBitsDinamica::iterator i=matrix.begin(); i!=matrix.end(); ++i) {
        int size = i->size();
        if(size > max) max = size;
    }

    // ajustar las filas para que todas tengan el maximo ancho
    for(MatrizDeBitsDinamica::iterator i=matrix.begin(); i!=matrix.end(); ++i) {
        i->resize(max);
    }

}

int contarSi(const Mascara& mascara, const bool valor) {

    int suma = 0;

    for(int x = 0; x < mascara.getAncho(); ++x) {
        for(int y = 0; y < mascara.getAlto(); ++y) {
            if(mascara.getValor(x, y) == valor) suma++;
        }
    }

    return suma;
}
