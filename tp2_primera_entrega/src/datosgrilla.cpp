#include <fstream>
#include <iostream>
#include <cstdlib> // para exit
#include "mascara.h"

#include "datosgrilla.h"
DatosGrilla::DatosGrilla(std::string rutaCatalogo, std::string rutaSecuencia) {
   // Cargar catalogo de piezas
   cargarCatalogo(rutaCatalogo);
   // Cargar secuencia
   cargarSecuencia(rutaSecuencia);
   // TODO: verificar que sean consistentes
}


void DatosGrilla::cargarSecuencia(const std::string &ruta){
    // secuencia temporal para no modificar la original hasta no terminar
    // la lectura
    Secuencia secuenciaTemporal;
    IdElementoSecuencia elementoSecuencia;

    // abrir archivo
    std::ifstream archivoSecuencia(ruta.c_str(), std::ifstream::in);
    if(!archivoSecuencia) { std::cerr << ruta.c_str() << " no pudo ser abierto" << std::endl; exit(1);}


    archivoSecuencia >> ancho;
    archivoSecuencia >> alto;

    while(!archivoSecuencia.eof()) {
        archivoSecuencia >> elementoSecuencia;
        secuenciaTemporal.push_back(elementoSecuencia);
    }

    // TODO: Fix this hack of duplicate last push_back
    secuenciaTemporal.pop_back();

    // cerrar archivo
    archivoSecuencia.close();

    // asignar nueva secuencia sobre la anterior.
    this->secuencia = secuenciaTemporal;
}

void DatosGrilla::cargarCatalogo(const std::string &ruta) {
    ::cargarCatalogo(&catalogo, ruta);
}


//Getters

int DatosGrilla::getAlto() const { return alto; }

int DatosGrilla::getAncho() const { return ancho; }

Secuencia DatosGrilla::getSecuencia() const { return secuencia;}

CatalogoUnicoMascaras DatosGrilla::getCatalogo() const { return catalogo; }

