#include <iostream>
#include "EasyBMP.h"
#include "representaciongrilla.h"
#include "celda.h"

Celda::Celda(){
    estaOcupada = false;  //color blanco
    color.Red = 255;
    color.Green = 255;
    color.Blue = 255;
    color.Alpha = 0;
    for (int i = 0; i<4; i++){
        bordes[i] = false;
    }
}


bool Celda::ocupada() const{
    return this->estaOcupada;
}


void Celda::ocupar(bool estaOcupada){
    this->estaOcupada = estaOcupada;
}


void Celda::setColor(RGBApixel color){
    this->color = color;
}


void Celda::setBorde(Celda::Lado lado, bool esBorde){
    switch (lado){
        case ARRIBA:
            this->bordes[0] = esBorde;
            break;
        case DERECHA:
            this->bordes[1] = esBorde;
            break;
        case ABAJO:
            this->bordes[2] = esBorde;
            break;
        case IZQUIERDA:
            this->bordes[3] = esBorde;
            break;
    }
}


RGBApixel Celda::colorBorde(Celda::Lado lado) const{
    RGBApixel bordeNegro;
    RGBApixel colorAUsar = this->color;
    // TODO:
    // BUG, depende de que no este inicializado:
    //esto deberia poder ser construido:
    //colorAUsar.Red = this->color.Red;
    // y Blue y Green
    // pero falla porque parece que el codigo depende de la
    // no inicializacion, reestructurar y corregir.
    bordeNegro.Red = 0; bordeNegro.Green = 0; bordeNegro.Blue = 0; bordeNegro.Alpha = 0;
    switch (lado){
        case ARRIBA:
            if (this->bordes[0]) colorAUsar = bordeNegro; break;
        case DERECHA:
            if (this->bordes[1]) colorAUsar = bordeNegro; break;
        case ABAJO:
            if (this->bordes[2]) colorAUsar = bordeNegro; break;
        case IZQUIERDA:
            if (this->bordes[3]) colorAUsar = bordeNegro; break;
    }
    return colorAUsar;
}


void Celda::graficarCelda(int x, int y, BMP& grafico, int tamanioCelda){
    int tamanioBorde = tamanioCelda/10;
    //BORDE SUPERIOR
    for (int i = x*tamanioCelda; i<(x+1)*tamanioCelda; i++)
        for (int j = y*tamanioCelda; j<y*tamanioCelda+tamanioBorde; j++)
            grafico.SetPixel(i, j ,colorBorde(ARRIBA));
    //BORDE DERECHO
    for (int i = (x+1)*tamanioCelda-tamanioBorde; i<(x+1)*tamanioCelda; i++)
        for (int j = y*tamanioCelda; j<(y+1)*tamanioCelda; j++)
            grafico.SetPixel(i, j ,colorBorde(DERECHA));
    //BORDE INFERIOR
    for (int i = x*tamanioCelda; i<(x+1)*tamanioCelda; i++)
        for (int j = (y+1)*tamanioCelda-tamanioBorde; j<(y+1)*tamanioCelda; j++)
            grafico.SetPixel(i, j ,colorBorde(ABAJO));
    //BORDE IZQUIERDO
    for (int i = x*tamanioCelda; i<x*tamanioCelda+tamanioBorde; i++)
        for (int j = y*tamanioCelda; j<(y+1)*tamanioCelda; j++)
            grafico.SetPixel(i, j ,colorBorde(IZQUIERDA));
    //INTERIOR DE LA CELDA
    for (int i = x*tamanioCelda+tamanioBorde; i < (x+1)*tamanioCelda-tamanioBorde; i++)  //LINEA A LINEA (VERTICAL)
        for (int j = y*tamanioCelda+tamanioBorde; j < (y+1)*tamanioCelda-tamanioBorde; j++)  //PIXEL A PIXEL
            grafico.SetPixel(i, j, this->color);
}

