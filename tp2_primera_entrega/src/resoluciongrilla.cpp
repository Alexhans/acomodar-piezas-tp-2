#include <algorithm>
#include <iostream>
#include <map>

#include "resoluciongrilla.h"
#include "datosgrilla.h"

#define DEBUG_INFO_RESOLUCION
#undef DEBUG_INFO_RESOLUCION

typedef std::pair<IdElementoSecuencia, Mascara> ParElementoMascara;

PiezaIngresada::PiezaIngresada(
            const IdElementoSecuencia& tipoPieza,
            const Mascara &mascara,
            const Posicion &posicion,
            const Transformacion &transformacion):
    mascara(mascara),
    posicion(posicion),
    transformacion(transformacion),
    tipoPieza(tipoPieza)
{
}

///////////////////////////////////////////////////////////////////////////////
//  RESOLUCIONGRILLA
///////////////////////////////////////////////////////////////////////////////

ResolucionGrilla::ResolucionGrilla() {
}

int ResolucionGrilla::getCantidadPiezasUsadas() const {
    return piezasIngresadas.size();
}

int ResolucionGrilla::getCantidadPiezasSinUsar() const {
    return piezasRestantes.size();
}

int ResolucionGrilla::getLugaresOcupados() const {
    return contarSi(mascaraFinal, true);
}

int ResolucionGrilla::getDimension() const {
    return mascaraFinal.getAncho() * mascaraFinal.getAlto();
}

ResolucionGrilla::ListaPiezasIngresadas& ResolucionGrilla::getPiezasIngresadas() {
    return piezasIngresadas;
}

ResolucionGrilla::ListaMascaras& ResolucionGrilla::getPiezasRestantes() {
    return piezasRestantes;
}

void ResolucionGrilla::setMascaraFinal(Mascara &masc) {
    mascaraFinal = masc;
}

Mascara ResolucionGrilla::getMascaraFinal() const {
    return mascaraFinal;
}


///////////////////////////////////////////////////////////////////////////////
// OBJETOS COMPARADORES
///////////////////////////////////////////////////////////////////////////////


struct compararPiezas {
    // si LEFT dimension > RIGHT dimension = true
    // si son iguales, LEFT alto > RIGHT alto
    //
    bool operator()(const ParElementoMascara &lhs, const ParElementoMascara &rhs) {
        int leftAlto = lhs.second.getAlto();
        int leftDim = leftAlto * lhs.second.getAncho();
        int rightAlto = rhs.second.getAlto();
        int rightDim = rightAlto * rhs.second.getAncho();
        if(leftDim > rightDim) {
            return true ;
        }
        else if(leftDim == rightDim) {
            return leftAlto > rightAlto;
        }
        return false; // leftDim < rightDim
    }
};

struct compararConMapa {
    std::map<IdElementoSecuencia, int> mapa;
    compararConMapa(const std::map<IdElementoSecuencia, int> mapa): mapa(mapa) {}
    bool operator()(const IdElementoSecuencia &lhs, const IdElementoSecuencia &rhs) {
        return mapa[lhs] < mapa[rhs];
    }
};

///////////////////////////////////////////////////////////////////////////////
// HELPERS para Resolucion
///////////////////////////////////////////////////////////////////////////////

void mostrarBarraDeProgreso(unsigned int num, unsigned int fraccion, char barra) {
    if(num > 10 && (num % fraccion == 0)) {
        std::cout << std::flush << barra<< barra<< barra<< barra;
    }
}

void ordenarSecuenciaEnBaseACatalogo(const CatalogoUnicoMascaras& catalogo,
                                     Secuencia &secuencia) {
    std::vector<ParElementoMascara> catalogoOrdenadoPorPieza(catalogo.begin(), catalogo.end());

    // ordena por dimension y luego por altura.
    std::sort(catalogoOrdenadoPorPieza.begin(), catalogoOrdenadoPorPieza.end(), compararPiezas());


    // hacer mapa de identificador y numero.
    int counter = 0;
    std::map<IdElementoSecuencia, int> mapaCatalogoOrdenado;
    for(std::vector<ParElementoMascara>::iterator it = catalogoOrdenadoPorPieza.begin();
        it != catalogoOrdenadoPorPieza.end(); ++it) {
          mapaCatalogoOrdenado[it->first] = counter;
          counter++;
    }

    // ordenar secuencia.
    compararConMapa comparadorConMapa(mapaCatalogoOrdenado);
    std::sort(secuencia.begin(), secuencia.end(), comparadorConMapa);
}


///////////////////////////////////////////////////////////////////////////////
// RESOLUCION DE ALGORITMO - Metodo panchicious (con transformaciones)
///////////////////////////////////////////////////////////////////////////////

/*
 * 1 - Ordena la secuencia de manera que primero ingrese los de mayor dimension y
 *  de ser iguales las dimensiones, el de mayor altura.
 *
 * 2 - Crea una mascara vacia (llena de false) de las dimensiones de la grilla deseada.
 *
 * 3 - Cicla por las secuencia de piezas probando si puede ingresarlas:
 *    prueba hasta agotar todas las transformaciones posibles
 *
 *    Con cada transformacion:
 *      Para probar un punto, debe cumplirse que el ancla de la pieza y la posicion
 *      en la que está intentando ingresar no esten ambas ocupadas.
 *
 *      ademas verifica que las dimensiones rectangular puedan ser contenidas
 *
 *      si se cumplen las condiciones, realiza el checkeo de colisiones
 *
 * 4 - En cuanto puede ingresar una pieza.  Lo hace y pasa a la siguiente pieza.
 *
 * 5 - Si no puede ingresar la pieza y ha recorrido todas las opciones.
 *      lo registra en las piezas Restantes.
 *
 * La solucion tendrá la mascara poblada con el resultado final y
 * los contenedores con la info de piezas ingresadas y restantes.
 *
 * Además, imprime una barra de progreso.
 */
ResolucionGrilla resolverAlgoritmoPanch(const DatosGrilla &datosGrilla) {
    ResolucionGrilla solucion;

    CatalogoUnicoMascaras catalogo = datosGrilla.getCatalogo();

    // aun no esta ordenada la secuencia pero lo va a estar.
    Secuencia secuenciaOrdenada = datosGrilla.getSecuencia();

    ordenarSecuenciaEnBaseACatalogo(catalogo, secuenciaOrdenada);

    // Ahora que ya tenemos el orden deseado.  Empezar a intentar la insercion de piezas.

    const int ALTO_GRILLA = datosGrilla.getAlto();
    const int ANCHO_GRILLA = datosGrilla.getAncho();

    Mascara GrillaFinal(ANCHO_GRILLA, ALTO_GRILLA);

    unsigned int fraccion = secuenciaOrdenada.size() / 10;  // usada para imprimir 10 % de progreso

    std::cout << "\n10% 20% 30% 40% 50% 60% 70% 80% 90% 100%\n";  // Referencia de progreso

    // el caracter lleno anda en windows pero no en linux
    // a pesar de estar en ASCII.  Por ahora, lo manejamos asi.
    unsigned char barra = '|';
    #ifdef _WIN64
        barra = 219;
    #elif _WIN32
        barra = 219;
    #endif

    // Matriz preparada para ciclar por las transformaciones
    const int CANTIDAD_DE_TRANSFORMACIONES = 6;
    Transformacion::Tipo tipoTrans[CANTIDAD_DE_TRANSFORMACIONES] = {
        Transformacion::ARRIBA,
        Transformacion::DERECHA,
        Transformacion::ABAJO,
        Transformacion::IZQUIERDA,
        Transformacion::ESPEJO_VERTICAL,
        Transformacion::ESPEJO_HORIZONTAL
    };
    short int contadorTrans = 0;  // usado para iterar por las transformaciones.

    for(unsigned int seqNum = 0; seqNum < secuenciaOrdenada.size(); ++seqNum) {

        mostrarBarraDeProgreso(seqNum, fraccion, barra);

        IdElementoSecuencia id = secuenciaOrdenada.at(seqNum);
        // Son dos mascaras diferentes porque a la mascaraTransformada la voy a modificar a
        // lo largo de los ciclos, pero con la otra, quiero saber cual ingreso.
        // Es una referencia por lo tanto no debería incurrir en un costo de copia.
        Mascara &mascaraQueIngresar = catalogo[id];
        Mascara mascaraTransformada(mascaraQueIngresar);

        Posicion posicionAIngresar;
        // Clase que realizará las transformaciones.  (comienza con arriba)
        Transformacion transformacionARealizar(Transformacion::ARRIBA);

        bool colisionan = true;
        int x = 0, y = 0;
        bool anclaYGrillaOcupdas = false;   // Usada para optimizar y evitar verificaciones de colision
        for(y = 0; y < ALTO_GRILLA; ++y) {
            for(x = 0; x < ANCHO_GRILLA; ++x) {

                colisionan = true;  // Por defecto asumir que colisionan

                posicionAIngresar = Posicion(x,y);

                //TODO panch ciclar por las transformaciones.
                contadorTrans = 0;

                // Asigna a la transformacion la mascara original para comenzar a manipularla.
                mascaraTransformada = mascaraQueIngresar;

                // Prueba las diferentes transformaciones hasta que alguna no colisione (osea
                // que puede ingresarla en la grilla) o se terminen las transformaciones.
                while(colisionan && contadorTrans < CANTIDAD_DE_TRANSFORMACIONES) {

                    transformacionARealizar.setTipo(tipoTrans[contadorTrans]);

                    // Transformo antes de verificar colisiones.
                    mascaraTransformada = transformacionARealizar.transformar(mascaraTransformada);

                    // OPTIMIZACION: si el ancla de la transformacion esta ocupada y y la posicion en la cual la quiere
                    // ingresar tambien lo esta.  No verifica nada mas.  Pasa a la siguiente transformacion.
                    //
                    anclaYGrillaOcupdas = GrillaFinal.getValor(x, y) && mascaraTransformada.getValor(0,0);

                    // Además, debe verificar que entre.
                    if( !anclaYGrillaOcupdas &&
                        x + mascaraTransformada.getAncho() <= ANCHO_GRILLA &&  // entra de alto y ancho
                        y + mascaraTransformada.getAlto() <= ALTO_GRILLA ) {

                        // verifica si colisionan
                        colisionan = checkisColisionan( mascaraTransformada, GrillaFinal, posicionAIngresar);
                    }
                    contadorTrans++;
                }
                // si no colisinan, puede ingresar las piezas, salir del doble ciclo
                if(!colisionan) break;  // TODO decidir si dejar los break (son legibles)
            }
            if(!colisionan) break;
        }
        // si no solicionan, ingresa la pieza
        if(!colisionan) {
            // Fijar la pieza en la mascara de grilla.
            GrillaFinal.superponer(mascaraTransformada, x, y);

            // construye y agrega la PiezaIngresada a la solución (en el contenedor STL)
            PiezaIngresada pieza(id, mascaraTransformada, posicionAIngresar, transformacionARealizar);
            solucion.getPiezasIngresadas().push_back(pieza);
        }
        // si recorre todo y no encuentra ubicacion, agrega la mascara al contenedor
        // piezas restantes
        else {
            solucion.getPiezasRestantes().push_back(mascaraQueIngresar);
        }
    }

    // una vez que terminó con todas las piezas.
    // Guardar la grilla final (Mascara) en la solucion.
    solucion.setMascaraFinal(GrillaFinal);

    return solucion;
}

///////////////////////////////////////////////////////////////////////////////
// RESOLUCION DE ALGORITMO - Metodo panchicious (sin transformaciones)
///////////////////////////////////////////////////////////////////////////////

// TODO, remover codigo duplicado entre los dos algoritmos de tipo panch
// y arreglarlo como al otro.
//
// PRIORIDAD: BAJA.  En este momento no se usa para nada mas que pruebas

ResolucionGrilla resolverAlgoritmoPanchSinTransformaciones(const DatosGrilla &datosGrilla) {
    ResolucionGrilla solucion;

    CatalogoUnicoMascaras catalogo = datosGrilla.getCatalogo();

    // aun no esta ordenada la secuencia pero lo va a estar.
    Secuencia secuenciaOrdenada = datosGrilla.getSecuencia();

    ordenarSecuenciaEnBaseACatalogo(catalogo, secuenciaOrdenada);

    // Ahora que ya tenemos el orden deseado.  Empezar a intentar la insercion de piezas.

    Mascara GrillaFinal(datosGrilla.getAncho(), datosGrilla.getAlto());

    for(unsigned int seqNum = 0; seqNum < secuenciaOrdenada.size(); ++seqNum) {
        IdElementoSecuencia id = secuenciaOrdenada.at(seqNum);
        Mascara mascaraQueIngresar = catalogo[id];
        bool colisionan = true;
        int offsetX = 0, offsetY = 0;
        Posicion posicionAIngresar;
        Transformacion transformacionARealizar(Transformacion::ARRIBA);

        for(int y=0; y<datosGrilla.getAlto(); ++y) {
            // antes de probar la colision fijarse si la solucion va a estar contenida
            // totalmente en la GrillaFinal.
            if(offsetY + mascaraQueIngresar.getAlto() > GrillaFinal.getAlto()) continue;
            for(int x=0; x<datosGrilla.getAncho(); ++x) {
                offsetX = x;
                offsetY = y;
                colisionan = true;
                // OPTIMIZACION: si el "ancla" esta ocupada, saltear
                if(GrillaFinal.getValor(offsetX, offsetY)) continue;
                // antes de probar la colision fijarse si la solucion va a estar contenida
                // totalmente en la GrillaFinal.
                if(offsetX + mascaraQueIngresar.getAncho() > GrillaFinal.getAncho()) continue;
                posicionAIngresar = Posicion(offsetX,offsetY);
                transformacionARealizar = Transformacion(Transformacion::ARRIBA);
                colisionan = checkisColisionan(  mascaraQueIngresar, GrillaFinal,
                                        posicionAIngresar, Posicion(0,0));
                //std::cout << "prueba con offset " << offsetX << ", " << offsetY << ": " << colisionan << std::endl;
                if(!colisionan) break;
            }
            if(!colisionan) break;
        }
        if(!colisionan) {
        GrillaFinal.superponer(mascaraQueIngresar, offsetX, offsetY);

        PiezaIngresada pieza(id, mascaraQueIngresar, posicionAIngresar, transformacionARealizar);
        solucion.getPiezasIngresadas().push_back(pieza);


    #ifdef DEBUG_INFO_RESOLUCION
        GrillaFinal.show();  // muestra poco a poco como va quedando la grilla.
    #endif
        }
        else {
            std::cout << "en todo punto colisionan con offset " << offsetX << ", " << offsetY << std::endl;
            solucion.getPiezasRestantes().push_back(mascaraQueIngresar);
        }
    }

    solucion.setMascaraFinal(GrillaFinal);
    return solucion;
}
