#include <iostream>
#include <fstream>
#include <vector>
#include "representaciongrilla.h"
#include "mascara.h"


const int TAMANIO_CELDA = 30;   //cantidad de pixels del lado de cada celda en el .bmp final

RepresentacionGrilla::RepresentacionGrilla(const ResolucionGrilla& resolucion){
    this->representacion.SetBitDepth(32);
    Mascara mascaraFinal = resolucion.getMascaraFinal();
    this->x = mascaraFinal.getAncho() * TAMANIO_CELDA;
    this->y = mascaraFinal.getAlto() * TAMANIO_CELDA;
    this->representacion.SetSize(x,y);
    this->resolucion = resolucion;

    //PALETA DE COLORES: blanco y negro para el fondo, la grilla y los bordes; distintos colores para cada tipo de pieza.
    RGBApixel blanco; blanco.Red=255; blanco.Green=255; blanco.Blue=255; blanco.Alpha = 0;
    paletaColores['0'] = blanco;
    RGBApixel negro; negro.Red=0; negro.Green=0; negro.Blue=0; negro.Alpha = 0;
    paletaColores['1'] = negro;
    RGBApixel fucsia; fucsia.Red=255; fucsia.Green=0; fucsia.Blue=255; fucsia.Alpha = 0;
    paletaColores['A'] = fucsia;
    RGBApixel gris; gris.Red=128; gris.Green=128; gris.Blue=128; gris.Alpha = 0;
    paletaColores['B'] = gris;
    RGBApixel rojo; rojo.Red=255; rojo.Green=0; rojo.Blue=0; rojo.Alpha = 0;
    paletaColores['C'] = rojo;
    RGBApixel marron; marron.Red=128; marron.Green=0; marron.Blue=0; marron.Alpha = 0;
    paletaColores['D'] = marron;
    RGBApixel amarillo; amarillo.Red=255; amarillo.Green=255; amarillo.Blue=0; amarillo.Alpha = 0;
    paletaColores['E'] = amarillo;
    RGBApixel oliva; oliva.Red=128; oliva.Green=128; oliva.Blue=0; oliva.Alpha = 0;
    paletaColores['F'] = oliva;
    RGBApixel verde; verde.Red=0; verde.Green=255; verde.Blue=0; verde.Alpha = 0;
    paletaColores['G'] = verde;
    RGBApixel verdeOscuro; verdeOscuro.Red=0; verdeOscuro.Green=128; verdeOscuro.Blue=0; verdeOscuro.Alpha = 0;
    paletaColores['H'] = verdeOscuro;
    RGBApixel celeste; celeste.Red=0; celeste.Green=255; celeste.Blue=255; celeste.Alpha = 0;
    paletaColores['I'] = celeste;
    RGBApixel turquesa; turquesa.Red=0; turquesa.Green=128; turquesa.Blue=128; turquesa.Alpha = 0;
    paletaColores['J'] = turquesa;
    RGBApixel azul; azul.Red=0; azul.Green=0; azul.Blue=255; azul.Alpha = 0;
    paletaColores['K'] = azul;
    RGBApixel azulMarino; azulMarino.Red=0; azulMarino.Green=0; azulMarino.Blue=128; azulMarino.Alpha = 0;
    paletaColores['L'] = azulMarino;
    RGBApixel violeta; violeta.Red=128; violeta.Green=0; violeta.Blue=128; violeta.Alpha = 0;
    paletaColores['N'] = violeta;
    RGBApixel grisClaro; grisClaro.Red=192; grisClaro.Green=192; grisClaro.Blue=192; grisClaro.Alpha = 0;
    paletaColores['M'] = grisClaro;
}


RepresentacionGrilla::~RepresentacionGrilla(){
}


void RepresentacionGrilla::dibujarPieza(PiezaIngresada pieza){
    //recorre la mascara de la pieza a ingresar pasada por parametro (ya rotada/espejada en la resolucion)
    int ancho = pieza.mascara.getAncho();
    int alto = pieza.mascara.getAlto();
    for (int i = 0; i<ancho; i++){
        for (int j = 0; j<alto; j++){
            //cada casillero de la mascara se traduce a una celda, con sus respectivos bordes (analizados en la misma
            // mascara) y el color correspondiente (el del tipo de pieza en las celdas ocupadas, blanco en las vacias)
            Celda celda;
            celda.ocupar(pieza.mascara.getValor(i,j));  //ocupa la celda si en la mascara, en esa posicion, hay una parte de
                                                        //una pieza (o sea, si getValor en esa posicion devuelve true).

            if (celda.ocupada()){   //dibuja unicamente las celdas de la mascara correspondientes a la pieza; no el espacio vacio
                //ASIGNACION DE BORDES
                bool bordeARRIBA;   //borde de arriba
                if (j>0){
                    bordeARRIBA = (!pieza.mascara.getValor(i,j-1));
                }else{
                    bordeARRIBA = true;
                }
                celda.setBorde(Celda::ARRIBA, bordeARRIBA);
                bool bordeDERECHA;  //borde derecho
                if (i<ancho-1){
                    bordeDERECHA = (!pieza.mascara.getValor(i+1,j));
                }else{
                    bordeDERECHA = true;
                }
                celda.setBorde(Celda::DERECHA, bordeDERECHA);
                bool bordeABAJO;    //borde de abajo
                if (j<alto-1){
                    bordeABAJO = (!pieza.mascara.getValor(i,j+1));
                }else{
                    bordeABAJO = true;
                }
                celda.setBorde(Celda::ABAJO, bordeABAJO);
                bool bordeIZQUIERDA;    //borde izquierdo
                if (i>0){
                    bordeIZQUIERDA = (!pieza.mascara.getValor(i-1,j));
                }else{
                    bordeIZQUIERDA = true;
                }
                celda.setBorde(Celda::IZQUIERDA, bordeIZQUIERDA);
                //DIBUJAR
                celda.setColor(paletaColores[pieza.tipoPieza]);
                celda.graficarCelda(pieza.posicion.getX()+i, pieza.posicion.getY()+j, this->representacion, TAMANIO_CELDA);
            }
        }
    }
}


void RepresentacionGrilla::exportarComoImagen(std::string ruta){
    std::cout << "Dibujando..." << std::endl;
    ResolucionGrilla::ListaPiezasIngresadas piezas = resolucion.getPiezasIngresadas();
    unsigned int cantidadPiezas = piezas.size();
    for (unsigned int i = 0; i<cantidadPiezas; i++){    //dibujar las piezas ingresadas una a una
        this->dibujarPieza(piezas.at(i));
    }
    for (int i = 0; i*TAMANIO_CELDA<x; i++){  //pinto lineas verticales de la cuadricula
        for (int j = 0; j<y; j++){
            representacion.SetPixel(i*TAMANIO_CELDA,j,paletaColores['1']);
        }
    }
    for (int j = 0; j*TAMANIO_CELDA<y; j++){  //pinto lineas horizontales de la cuadricula
        for (int i = 0; i<x; i++){
            representacion.SetPixel(i,j*TAMANIO_CELDA,paletaColores['1']);
        }
    }
    representacion.WriteToFile(ruta.c_str());   //imprimir la imagen a un archivo
}
