-- will create the folders according to the action (gmake, vs2010, etc) and os
--bin_os_compiler = "bin/" .. os.get() .. "/" .. _ACTION
bin_os_compiler = "bin/" .. _ACTION

-- A solution contains projects, and defines the available configurations
solution "acomodar"
   configurations { "Debug", "Release" }
   --location (bin_os_compiler) 
 
   -- A project defines one build target
   project "acomodar"
      kind "ConsoleApp"
      language "C++"
      files { "inc/**.h", "src/**.cpp" }
      --location (bin_os_compiler) 
      objdir (bin_os_compiler) 
      --targetdir (bin_os_compiler) 
      includedirs { "inc" }

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols" }
         targetsuffix "_debug"
 
      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize" }    
         targetsuffix "_release"

      configuration { "gmake" }
          buildoptions { "-g", "-Wall", "-pedantic"}

      --configuration { "Debug", "gmake" }
      --    buildoptions { "-o0", "-g" }





