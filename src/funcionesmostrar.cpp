#include <iostream>
#include <vector>
#include <string>
#include <map>

#include "casillero.h"
#include "catalogo.h"
#include "tablero.h"
#include "mascara.h"

//TODO (Quizas) agregar las funciones de muestra de mascara [show()] y las de encastrador [mostrarMensajeBienvenida(),
//  mostrarInfoDatosCargados(), mostrarOcupacionYPiezasUsadas()].

//TODO agregar la funcion de mostrarTablero() cuando este hecha usando iteradores.

void Casillero::debug_mostrar() const {
    using namespace std;
    if(piezaEnCasillero) {
        piezaEnCasillero->debug_mostrar();
    }
    else {
        cout << "No hay pieza" << endl;
    }
    cout << "coordenadas relativas: " << endl;
    cout << "(" << coordXRel << ", " << coordYRel << ")" << endl;
}


void mostrarCatalogo(const Catalogo &cat) {
    Catalogo::CatalogoUnicoMascaras catalogo = cat.getCatalogo();
    for(Catalogo::CatalogoUnicoMascaras::iterator it = catalogo.begin();
        it != catalogo.end(); ++it) {
        std::cout << it->first << ":" << std::endl;
        it->second.debug_mostrar();
        std::cout << std::endl;
    }
}
