#include "encastrador.h"
#include <iostream>
#include <algorithm>

/*Pos: Inicializa las rutas de los archivos de Secuencia, Catalogo e Imagen
* en valores por defecto.
*/
Encastrador::Encastrador(std::string sec, std::string cat):
    archivoCatalogo(cat),
    archivoSecuencia(sec),
    catalogo(cat),
    secuencia(sec),
    tablero(secuencia.getAncho(), secuencia.getAlto()),
    graficador(),
    rutaImagen("resolucion.bmp") {
}

Encastrador::~Encastrador() {
    //dtor
}

void Encastrador::setRutaImagen(std::string rutaImagen) {
    this->rutaImagen = rutaImagen;
}

void Encastrador::resolver(Encastrador::tipoResolucion algoritmo) {
    if (algoritmo == TP1) {
        std::cout << std::endl << "Resolviendo con algoritmo del TP1, por favor espere...";
        this->resolverTP1();
        std::cout << " Resuelto el TP1" << std::endl;
        setRutaImagen("resolucion_tp1.bmp");
    }
    if (algoritmo == Panch) {
        std::cout << std::endl << std::endl << "Resolviendo con algoritmo del TP2, por favor espere...";
        this->resolverTP2();
        std::cout << " Resuelto el TP2" << std::endl;
        setRutaImagen("resolucion_panch.bmp");
    }
}

void Encastrador::graficar() {
    this->graficador.graficar(&tablero);
    this->graficador.exportarImagen(this->rutaImagen);
    std::cout << std::endl << "La imagen donde se encuentra la solucion esta en la misma carpeta que el ejecutable con el nombre de: " << this->rutaImagen << std::endl;
}

void Encastrador::mostrarMensajeBienvenida() {
    std::cout << "Bienvenido a la resolucion del Trabajo Practico 2 de la \ncatedra Calvo de Algoritmos y Programacion 2." << std::endl << std::endl;
        std::cout << "Los integrantes del grupo son: Gaston Goffredo, Alex Guglielmone Nemi, \nTobias Lichtig, Santiago Acuna y Francisco Landino." << std::endl << std::endl;
        std::cout << "Grupo 8, 1er Cuatrimestre 2013. Corrector: Gustavo Schmidt." << std::endl << std::endl;

        std::cout << "------------------------------------------------------------" << std::endl << std::endl;

        std::cout << "Cargando catalogo de piezas desde: " << archivoCatalogo << std::endl << std::endl;
        std::cout << "Cargando secuencia de piezas desde: " << archivoSecuencia << std:: endl << std:: endl;
}

void Encastrador::mostrarInfoDatosCargados() {
    mostrarDimensiones(this->tablero);
    mostrarSecuencia(this->secuencia);
}

void Encastrador::representar() {
   mostrarOcupacionYPiezasUsadas();
   graficar();
}

void Encastrador::mostrarOcupacionYPiezasUsadas() {
    int ocupacion = tablero.obtenerCantidadDeLugaresOcupados(); //solucion.getLugaresOcupados();
    int dimension = tablero.obtenerAncho() * tablero.obtenerAlto(); //solucion.getDimension();
    double factor_de_ocupacion = ocupacion / static_cast<double>(dimension);
    int piezasUtilizadas = tablero.obtenerCantidadDePiezasAgregadas();
    std::cout << "factor: " << factor_de_ocupacion << " - " << factor_de_ocupacion*100 << " % ";
    std::cout << "piezas utilizadas: " << piezasUtilizadas << std::endl;
    std::cout << "piezas sin usar: " << secuencia.getSecuencia().size() - piezasUtilizadas << std::endl << std::endl;

}

void Encastrador::mostrarSecuencia(const Secuencia& secuencia) {
    std::cout<<"SECUENCIA: "<<std::endl;
    for (int i=0; i < secuencia.getCantidadDeElementos(); i++) {
        std::cout<<secuencia.getSecuencia().at(i);
    }
    std::cout<<std::endl<<"Total: "<<secuencia.getCantidadDeElementos()<<" piezas."<<std::endl;
    std::cout<<std::endl;
}

void Encastrador::mostrarDimensiones(Tablero tablero) {
    std::cout<<"Dimensiones del tablero"<<std::endl;
    std::cout<<"Alto: "<<tablero.obtenerAlto()<<std::endl;
    std::cout<<"Ancho: "<<tablero.obtenerAncho()<<std::endl;
}
///////////////////////////////////////////////////////////////////////////////
// OBJETOS COMPARADORES
///////////////////////////////////////////////////////////////////////////////

struct compararPiezas {
    // si LEFT dimension > RIGHT dimension = true
    // si son iguales, LEFT alto > RIGHT alto
    //
    bool operator()(const ParElementoPieza &lhs, const ParElementoPieza &rhs) {
        int leftAlto = lhs.second.getAlto();
        int leftDim = leftAlto * lhs.second.getAncho();
        int rightAlto = rhs.second.getAlto();
        int rightDim = rightAlto * rhs.second.getAncho();
        if(leftDim > rightDim) {
            return true ;
        }
        else if(leftDim == rightDim) {
            return leftAlto > rightAlto;
        }
        return false; // leftDim < rightDim
    }
};

struct compararConMapa {
    std::map<char, int> mapa;
    compararConMapa(const std::map<char, int> mapa): mapa(mapa) {}
    bool operator()(const char &lhs, const char &rhs) {
        return mapa[lhs] < mapa[rhs];
    }
};


///////////////////////////////////////////////////////
//// NON MEMBER FUNCTION
///////////////////////////////////////////////////////

std::vector<char> ordenarSecuenciaEnBaseACatalogo(const CatalogoUnicoPieza& catalogo,
                                     const std::vector<char> &secuencia) {

    std::vector<char> secuenciaOrdenada=secuencia;
    std::vector<ParElementoPieza> catalogoOrdenadoPorPieza(catalogo.begin(), catalogo.end());

    // ordena por dimension y luego por altura.
     std::sort(catalogoOrdenadoPorPieza.begin(), catalogoOrdenadoPorPieza.end(), compararPiezas());


    // hacer mapa de identificador y numero.
    int counter = 0;
    std::map<char, int> mapaCatalogoOrdenado;
    for(std::vector<ParElementoPieza>::iterator it = catalogoOrdenadoPorPieza.begin();
        it != catalogoOrdenadoPorPieza.end(); ++it) {
          mapaCatalogoOrdenado[it->first] = counter;
          counter++;
    }

    // ordenar secuencia.
    compararConMapa comparadorConMapa(mapaCatalogoOrdenado);
    std::sort(secuenciaOrdenada.begin(), secuenciaOrdenada.end(), comparadorConMapa);
    return secuenciaOrdenada;
}


