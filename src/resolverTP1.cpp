#include "encastrador.h"
#include <iostream>

void Encastrador::resolverTP1(){

    tablero.vaciarTablero();

    const int ANCHO_TABLERO = tablero.obtenerAncho();
    const int ALTO_TABLERO  = tablero.obtenerAlto();

    int i = 0;  //posicion de la pieza en la secuencia
    bool entra_de_alto = true;
    int alturaFila = 0; //altura maxima de las piezas en una fila (para hacer los saltos de linea).
    int x = 0; //proxima posicion en x en el donde probar insertar la siguiente pieza.
    int y = 0; //altura a la que se probara insertar la proxima pieza.
    int longitudSecuencia = secuencia.getCantidadDeElementos();

    while (entra_de_alto &&      // entra de alto (si no entra dejar de ingresar)
            i < longitudSecuencia    // quedan piezas por poner
            )
    {
        bool isPiezaIngresada = false;

        char nombrePieza = secuencia.getSiguienteDeSecuencia(i);

        Pieza* piezaAIngresar = &catalogo.getPieza(nombrePieza)->second;

        const int ALTO_PIEZA = piezaAIngresar->getAlto();
        const int ANCHO_PIEZA = piezaAIngresar->getAncho();

        entra_de_alto = (y + ALTO_PIEZA <= ALTO_TABLERO);
        bool entra_de_ancho = (x + ANCHO_PIEZA <= ANCHO_TABLERO);

        while (!isPiezaIngresada && entra_de_alto && entra_de_ancho){

            bool esPosibleAgregar = tablero.esPosibleAgregar(piezaAIngresar, x, y);

            if (esPosibleAgregar){

                tablero.agregar(piezaAIngresar, x, y); //inserto la pieza en la grilla en la posicion (x,y)
                isPiezaIngresada = true;  // esto quiere decir que saldra del loop

                int alturaTemporal = y + ALTO_PIEZA - 1;    //se fija si crecio la altura de la fila
                if (alturaTemporal > alturaFila){
                    alturaFila = alturaTemporal;
                }
                x += ANCHO_PIEZA;   // corre todo el cuadrado de la pieza

            }else{          // si no se puede agregar, mover a la derecha o a la linea adecuada de abajo.
                x++;
                entra_de_ancho = (x + ANCHO_PIEZA <= ANCHO_TABLERO);
                if(!entra_de_ancho){
                    x = 0;
                    y = alturaFila + 1;
                }
            }

            entra_de_alto = (y + ALTO_PIEZA <= ALTO_TABLERO);
        }

        // si no entraba de ancho, bajar una fila.
        if(!entra_de_ancho){
            x = 0;
            y = alturaFila + 1;
        }else{
            i++;
        }
    }
}
