#include "borde.h"

Borde::Borde() {
    for(int i=0;i<4;++i) {
        esBorde[i] = false;
    }
}
void Borde::setBorde(Borde::Lado lado, bool valor) {
    esBorde[lado] = valor;
}

Borde::~Borde() {
    //dtor
}

bool Borde::hayBorde(Borde::Lado lado) const {
    return esBorde[lado];
}
