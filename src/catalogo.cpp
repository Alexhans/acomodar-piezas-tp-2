#include "catalogo.h"
#include "pieza.h"
#include <cstdlib>

Catalogo::Catalogo(const std::string &ruta){
    //ctor
    cargarCatalogo(&catalogo, ruta);
    //mostrarCatalogo(*this);
}

Catalogo::~Catalogo(){
    //dtor
}

void Catalogo::cargarCatalogo(Catalogo::CatalogoUnicoMascaras *catalogo, std::string archivo) {
    catalogo->clear();

    std::ifstream file(archivo.c_str(), std::ifstream::in);
    if(!file) { std::cerr << archivo.c_str() << " no pudo ser abierto" << std::endl; exit(1); }

    /*
    while not eof:
        read number of lines
        while not eof and count < lines:
            load non space as 1 and space as 0
            push_back
            verify current width
    */
    // TODO: read everything once
    char identificadorElemento = 'A';
    while(!file.eof()) {

        int lines = 0;
        file >> lines; // TODO check existence

        // IMPORTANT
        MatrizDeBitsDinamica matrix;
        matrix.resize(lines);

        int counter = 0;
        std::string charline = "";
        safeGetline(file, charline); // automaticamente maneja las diferencias CRLF y LF (win/*nix).

        while((counter < lines) && (true)) {

            safeGetline(file, charline);
            // IMPORTANT
            for(std::string::iterator it=charline.begin();
                it!=charline.end();
                ++it) {
                    bool value = (*it)!=' '; // spaces are false
                    matrix.at(counter).push_back(value);
                }

            counter++;
        }

        completarRectangulo(matrix);

        // Agregarlo al mapa con el identificador apropiado
        Mascara masc(matrix);
        Pieza piezaParaAgregar(identificadorElemento, masc);
        catalogo->insert( std::pair<char, Pieza>( identificadorElemento, piezaParaAgregar ) );
        identificadorElemento++;

    }
    file.close();
}


Catalogo::CatalogoUnicoMascaras Catalogo::getCatalogo() const { return catalogo; }


Catalogo::CatalogoUnicoMascaras::iterator Catalogo::getPieza(char nombrePieza) {
    Catalogo::CatalogoUnicoMascaras::iterator it = catalogo.find(nombrePieza);
    return it;
}



