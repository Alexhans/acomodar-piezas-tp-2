#include "mascara.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib> // para exit

#include "posicion.h"


#define DEBUG_CHECK_COLISION
#undef DEBUG_CHECK_COLISION

///////////////////////////////////////////////////////////////////////////////
//  MASCARA
///////////////////////////////////////////////////////////////////////////////

Mascara::Mascara(unsigned int ancho, unsigned int alto) {
    data.resize(alto);
    for(unsigned int i=0; i<alto; ++i) {
        data.at(i).resize(ancho, false);
    }
}

Mascara::Mascara(const MatrizDeBitsDinamica &matrix):data(matrix) { }

Mascara::Mascara(const Mascara &that):data(that.data) {}

Mascara& Mascara::operator=(const Mascara& that) {
    data = that.data;
    return *this;
}

Mascara::~Mascara() {
    // los contenedores de STL se liberan por si mismos cuando salen de scope.
}

bool operator==(const Mascara& lhs, const Mascara& rhs) {
    int alto = lhs.getAlto();
    int ancho = lhs.getAncho();

    // si las dimensiones son diferentes no pueden ser iguales
    if(alto != rhs.getAlto() || ancho != rhs.getAncho()) {
        return false;
    }

    // Recorre todos los valores de ambos, si encuentra que en algun punto
    // los casilleros no coinciden.  No son iguales.
    bool sonIguales = true;
    for(int x=0; x<ancho; ++x) {
        for(int y=0; y<alto; ++y) {
            if(rhs.getValor(x,y) != lhs.getValor(x,y)) {
                sonIguales = false;
                break;
            }
        }
    }
    return sonIguales;
}

bool operator!= (const Mascara& lhs, const Mascara& rhs) {
    return !operator==(lhs, rhs);
}

int Mascara::getAlto() const { return data.size(); }

int Mascara::getAncho() const { if(data.size() > 0) return data.at(0).size();
return 0;}

// Atencion: No cambiar el orden a x,y.  Es y,x es correcto por como esta
// armada internamente la matriz.
bool Mascara::getValor(const int x, const int y) const {
    return data.at(y).at(x);
}

// Atencion: No cambiar el orden a x,y.  Es y,x es correcto por como esta
// armada internamente la matriz.
void Mascara::setValor(const int x, const int y, bool valor) {
    data.at(y).at(x) = valor;
}

SetDeBitsDinamico& Mascara::operator[] (const int indice) {
    return data.at(indice);
}

///////////////////////////////////////////////////////////////////////////////
//  SOURCE ONLY HELPER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////

// pos: ordenado por (x,y)  (0,0) (0,1) (1,0) (1,1)
std::vector<Posicion> generarPuntosRectanguloContenedor(const Posicion &offset, const Posicion &dimension) {
    std::vector<Posicion> puntos;
    int ancho = dimension.getX();
    int alto = dimension.getY();
    int offset_x = offset.getX();
    int offset_y = offset.getY();
    for(int y = 0; y < alto; ++y)
        for(int x = 0; x < ancho; ++x)
            puntos.push_back(Posicion(x + offset_x, y + offset_y));
    return puntos;
}

void debugMostrar(const std::vector<Posicion> pos) {
        for(std::vector<Posicion>::const_iterator it = pos.begin();
                it!=pos.end(); ++it) {
            std::cout << (*it) << std::endl;
        }
}


///////////////////////////////////////////////////////////////////////////////
//  NON MEMBER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////


//obtenida de : http://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf/6089413#6089413
std::istream& safeGetline(std::istream& is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}


void completarRectangulo(MatrizDeBitsDinamica &matrix) {

    int max = 0;

    // fijarse cual es la fila mas grande
    for(MatrizDeBitsDinamica::iterator i=matrix.begin(); i!=matrix.end(); ++i) {
        int size = i->size();
        if(size > max) max = size;
    }

    // ajustar las filas para que todas tengan el maximo ancho
    for(MatrizDeBitsDinamica::iterator i=matrix.begin(); i!=matrix.end(); ++i) {
        i->resize(max);
    }

}

int contarSi(const Mascara& mascara, const bool valor) {

    int suma = 0;

    for(int x = 0; x < mascara.getAncho(); ++x) {
        for(int y = 0; y < mascara.getAlto(); ++y) {
            if(mascara.getValor(x, y) == valor) suma++;
        }
    }

    return suma;
}

void Mascara::show(bool coordenadas) const{
    int count = 0;
    if(coordenadas) {
        std::cout << "  " << "\t" << "01234567890" << std::endl << std::endl;
    }
    for(MatrizDeBitsDinamica::const_iterator i = data.begin();
        i != data.end();
        ++i)
    {
        if(coordenadas) {std::cout << count << "\t"; }
        for(SetDeBitsDinamico::const_iterator j = i->begin();
        j != i->end();
        ++j)
        {
            std::cout << (*j);
        }
        count++;
        std::cout << std::endl;
    }
}

