#include "pieza.h"

Pieza::Pieza(char nombre, const Mascara &mascara):
    nombre(nombre),
    mascara(mascara),
    borde(0)
{
    calcularBordes();
}

Pieza::~Pieza(){}

void Pieza::calcularBordes() {
    /* Primero ajusta el tama�o de la matriz borde al tama�ano de la pieza.
       Luego se va fijando celda por celda de la pieza si esta ocupado y vuelca los valores apropiados al borde.
       Para definir si tiene borde, se fija si se encuentra en algun extremo de la pieza(porque eso significaria que termina la pieza)
       o se fija si el casillero de al lado se encuentra desocupado(!estaOcupado).
       Si alguno de los dos valores da verdadero, se le define un borde a ese lado.
       */
    const int ANCHO_PIEZA = this->getAncho();
    const int ALTO_PIEZA = this-> getAlto();
    borde.resize(ANCHO_PIEZA);
    for (int i = 0; i<ANCHO_PIEZA; i++){
        borde[i].resize(ALTO_PIEZA);
    }
    for (int i = 0; i<ANCHO_PIEZA; i++){    //Se mueve en ancho sobre la matriz de bordes
        for (int j = 0; j<ALTO_PIEZA; j++){ //Se mueve en alto sobre la matriz de bordes
            if (this->estaOcupado(i,j)){
                borde[i][j].setBorde(Borde::SUPERIOR, (j==0) || !estaOcupado(i,j-1));
                borde[i][j].setBorde(Borde::DERECHO, (i==ANCHO_PIEZA-1) || !estaOcupado(i+1,j));
                borde[i][j].setBorde(Borde::INFERIOR, (j==ALTO_PIEZA-1) || !estaOcupado(i,j+1));
                borde[i][j].setBorde(Borde::IZQUIERDO, (i==0) || !estaOcupado(i-1,j));
            }
        }
    }
}

bool Pieza::estaOcupado(int x, int y) const {
    return mascara.getValor(x,y);
}

int Pieza::getAlto() const {
    return mascara.getAlto();
}

int Pieza::getAncho() const {
    return mascara.getAncho();
}

Borde Pieza::getBorde(int x, int y, Transformacion::Tipo transformacionAplicada) {
    /* Crea un nuevo objeto Borde.
       Luego toma la mascara(de la pieza) sobre la cual se le va a calcular el borde y la transforma segun la
       transformacionAplicada.
       Crea una nueva pieza con la mascaraTransformada como parametro y luego le calcula los bordes a esa nueva pieza.
       Finalmente vuelca los bordes obtenidos al objeto creado al principio y los devuelve.
    */
    Borde result;
    Transformacion transformacionARealizar(transformacionAplicada);
    Mascara mascaraTransformada = transformacionARealizar.transformar(this->getMascara());
    Pieza nuevaPieza('Z', mascaraTransformada);
    result = nuevaPieza.getBorde(x,y);
    return result;
}

// Este getborde sirve para obtener el borde de la mascara de la nueva pieza ya transformada
Borde Pieza::getBorde(int x, int y) {
    Borde result;
    if((x < getAncho()) && (y < getAlto())){
        result = borde[x][y];
    }
    return result;

}

char Pieza::getNombre() const {
    return nombre;
}

void Pieza::debug_mostrar() {
    mascara.show();
}

Mascara& Pieza::getMascara() {
    return mascara;
}
