#include <iostream>
#include "encastrador.h"

int main(int argc, char **argv) {

    std::string archivoCatalogo = "data/catalogo/default.txt";
    std::string archivoSecuencia = "data/secuencias/a.txt";
    if(argc > 1) {
        archivoSecuencia = argv[1];
        if(argc > 2) {
            archivoCatalogo = argv[2];
        }
    }

///////////////////////////////////
//// CARGAR DATOS
///////////////////////////////////

    Encastrador encastrador(archivoSecuencia, archivoCatalogo);

    encastrador.mostrarMensajeBienvenida();

///////////////////////////////////
//// MOSTRAR INFO DATOS CARGADOS
///////////////////////////////////

    encastrador.mostrarInfoDatosCargados();

///////////////////////////////////
//// RESOLVER Y REPRESENTAR
///////////////////////////////////

    encastrador.resolver(Encastrador::Panch);

    encastrador.representar();

    encastrador.resolver(Encastrador::TP1);

    encastrador.representar();

}
