#include "tablero.h"
#include "pieza.h"
#include <iterator>
#include <list>

Tablero::Tablero(const int ancho, const int alto):cantidadPiezasAgregadas(0) {
    cambiarDimension(ancho, alto);
}

Tablero::~Tablero()  {
    //dtor
}

void Tablero::agregar(Pieza* pieza, int x, int y, Transformacion::Tipo transformacion) {
    // Esto es el equivalente a superponer por ahora.
    // agrega en los casilleros que puede, la pieza correcta.
    // transforma, de ser necesario:
    // a partir de x,y en casillero
    // usando la mascara de pieza.getMascara
    // hasta donde pueda de getAlto() y getAncho() usa
    // casillero.agregar si hay true en la posicion adecuada de mascara.
    //std::cout << "intentado agregar pieza\n";
    if(pieza) {
        //std::cout << "nombre: " << pieza->getNombre() << std::endl;
        //pieza->debug_mostrar();
        Transformacion transformacionARealizar(transformacion);
        Mascara mascaraAInsertar = transformacionARealizar.transformar(pieza->getMascara());
        //Mascara mascara(pieza->getMascara());
        for(int i = 0; i < mascaraAInsertar.getAncho(); ++i) {
            for(int j = 0; j < mascaraAInsertar.getAlto(); ++j) {
                if(mascaraAInsertar.getValor(i, j)) {
                    listaCasillero.getElementoAt(y+j).getElementoAt(x+i).agregarPieza(pieza, i, j, transformacion);
                }
            }
        }
        cantidadPiezasAgregadas++;
    }
    // mostrarTablero(*this);
}

bool Tablero::esPosibleAgregar(Pieza* pieza, int x, int y, Transformacion::Tipo transformacion) {

    /* Primero transforma la pieza pasada por argumento.
       Despues chekea que la pieza no se quiera colocar fuera de los limites del tablero.
       Luego va comparando casillero por casillero si la mascara transformada trataria de ocupar un casillero que ya esta ocupado.
       Si en algun momento esto sucede el valor esPosibleAgregar es seteado como false y es devuelto.
       Si supera todos los chequeos el valor esPosibleAgregar queda seteado como true y es devuelto.
    */
    bool esPosibleAgregar=true;
    Transformacion transformacionARealizar(transformacion);
    Mascara mascaraAInsertar = transformacionARealizar.transformar(pieza->getMascara());

    if ( ( (x+mascaraAInsertar.getAncho()) > (this->obtenerAncho()) ) || ( (y+mascaraAInsertar.getAlto()) > (this->obtenerAlto()) ) ){
        esPosibleAgregar = false;
    }

    for(int i = 0; (i < mascaraAInsertar.getAncho()) && (esPosibleAgregar); ++i) { // Recorre la mascara en ancho
        for(int j = 0; j < mascaraAInsertar.getAlto() && (esPosibleAgregar); ++j) {// Recorre la mascara en alto

            if(mascaraAInsertar.getValor(i,j)){
                Casillero casilleroTablero = this->obtenerCasillero(x+i,y+j);
                /*Si el casillero tiene una pieza devuelve un puntero diferente de NULL y esPosibleAgregar es false.
                 *Si el casillero esta vacio devuelve NULL y esPosibleAgregar es true.
                 */
                esPosibleAgregar = (casilleroTablero.obtenerPieza() == NULL);
            }
        }
    }
    return esPosibleAgregar;
}

Casillero& Tablero::obtenerCasillero(int x, int y){
    return listaCasillero.getElementoAt(y).getElementoAt(x);
}

int Tablero::obtenerAlto() const{
    return listaCasillero.size();
}

int Tablero::obtenerAncho() {
    int ancho = 0;
    if(listaCasillero.size() > 0) {
        ancho = listaCasillero.getElementoAt(0).size();
    }
    return ancho;
}

int Tablero::obtenerCantidadDeLugaresOcupados() {
    int cantidadDeLugaresOcupados = 0;
    int alto = obtenerAlto();
    int ancho = obtenerAncho();
    int i,j;

    for(i=0; i<alto; i++){
        for (j=0; j<ancho; j++){
            if (((this->obtenerCasillero(j, i)).obtenerPieza()) != NULL){
                cantidadDeLugaresOcupados ++;
            }
        }
    }

    return cantidadDeLugaresOcupados;
}

int Tablero::obtenerCantidadDePiezasAgregadas() const {
    return cantidadPiezasAgregadas;
}

void Tablero::cambiarDimension(int ancho, int alto) {
    listaCasillero.resize(alto);
    for(IteradorLista<Lista<Casillero> > iter(listaCasillero); iter.isNotDone(); iter.next()) {
        iter.get().resize(ancho);
    }
}

void mostrarTablero(Tablero& tablero) {
    char valor;
    for(int y=0;y<tablero.obtenerAlto();++y) {
        for(int x=0;x<tablero.obtenerAncho();++x) {
            valor = '0';
            if(tablero.obtenerCasillero(x,y).obtenerPieza()) {
                valor = '1';
            }
            std::cout << valor;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Tablero::vaciarTablero() {
    for(int x = 0; x < this->obtenerAncho(); ++x){

        for(int y = 0; y < this->obtenerAlto(); ++y){
            this->obtenerCasillero(x,y).vaciar();
        }
    }
    cantidadPiezasAgregadas = 0;
}

