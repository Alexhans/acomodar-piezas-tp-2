#include "graficador.h"

int Graficador::TAMANIO_CELDA = 30;

Graficador::Graficador(){
    this-> tamanioCelda = Graficador::TAMANIO_CELDA;
    grafico.SetBitDepth(32);
    this-> inicializarPaletaColores();
}


void Graficador::inicializarPaletaColores(){
    //PALETA DE COLORES: blanco para el fondo; negro para la grilla y los bordes; distintos colores para cada tipo de pieza.
    RGBApixel blanco; blanco.Red=255; blanco.Green=255; blanco.Blue=255; blanco.Alpha=0;
    paletaColores['0'] = blanco;
    RGBApixel negro; negro.Red=0; negro.Green=0; negro.Blue=0; negro.Alpha=0;
    paletaColores['1'] = negro;
    RGBApixel fucsia; fucsia.Red=255; fucsia.Green=0; fucsia.Blue=255; fucsia.Alpha=0;
    paletaColores['A'] = fucsia;
    RGBApixel gris; gris.Red=128; gris.Green=128; gris.Blue=128; gris.Alpha=0;
    paletaColores['B'] = gris;
    RGBApixel rojo; rojo.Red=255; rojo.Green=0; rojo.Blue=0; rojo.Alpha=0;
    paletaColores['C'] = rojo;
    RGBApixel marron; marron.Red=128; marron.Green=0; marron.Blue=0; marron.Alpha=0;
    paletaColores['D'] = marron;
    RGBApixel amarillo; amarillo.Red=255; amarillo.Green=255; amarillo.Blue=0; amarillo.Alpha=0;
    paletaColores['E'] = amarillo;
    RGBApixel oliva; oliva.Red=128; oliva.Green=128; oliva.Blue=0; oliva.Alpha=0;
    paletaColores['F'] = oliva;
    RGBApixel verde; verde.Red=0; verde.Green=255; verde.Blue=0; verde.Alpha=0;
    paletaColores['G'] = verde;
    RGBApixel verdeOscuro; verdeOscuro.Red=0; verdeOscuro.Green=128; verdeOscuro.Blue=0; verdeOscuro.Alpha=0;
    paletaColores['H'] = verdeOscuro;
    RGBApixel celeste; celeste.Red=0; celeste.Green=255; celeste.Blue=255; celeste.Alpha=0;
    paletaColores['I'] = celeste;
    RGBApixel turquesa; turquesa.Red=0; turquesa.Green=128; turquesa.Blue=128; turquesa.Alpha=0;
    paletaColores['J'] = turquesa;
    RGBApixel azul; azul.Red=0; azul.Green=0; azul.Blue=255; azul.Alpha=0;
    paletaColores['K'] = azul;
    RGBApixel azulMarino; azulMarino.Red=0; azulMarino.Green=0; azulMarino.Blue=128; azulMarino.Alpha=0;
    paletaColores['L'] = azulMarino;
    RGBApixel violeta; violeta.Red=128; violeta.Green=0; violeta.Blue=128; violeta.Alpha=0;
    paletaColores['N'] = violeta;
    RGBApixel grisClaro; grisClaro.Red=192; grisClaro.Green=192; grisClaro.Blue=192; grisClaro.Alpha=0;
    paletaColores['M'] = grisClaro;
}


void Graficador::graficar(Tablero* tablero){
    std::cout << "Dibujando...";
    this->cantCeldasDeAncho = tablero->obtenerAncho();
    this->cantCeldasDeAlto = tablero->obtenerAlto();
    grafico.SetSize(cantCeldasDeAncho*tamanioCelda, cantCeldasDeAlto*tamanioCelda);
    for (int y = 0; y < this->cantCeldasDeAlto; y++){
        for (int x = 0; x < this->cantCeldasDeAncho; x++){
            graficarCasillero(tablero,x,y);
        }
    }
    this-> graficarCuadricula();
    std::cout << " Dibujo finalizado." << std::endl;
}


void Graficador::graficarCasillero(Tablero* tablero, int x, int y){
    int tamanioBorde = tamanioCelda/10;
    Casillero casilleroAGraficar = tablero->obtenerCasillero(x,y);
    Pieza* pieza = casilleroAGraficar.obtenerPieza();
    RGBApixel colorCasillero = paletaColores['0'];
    Borde borde;
    if(pieza) {
        colorCasillero = paletaColores[pieza->getNombre()];
        borde = pieza->getBorde(casilleroAGraficar.obtenerCoordenadaXRelativa(),casilleroAGraficar.obtenerCoordenadaYRelativa(), casilleroAGraficar.getTransformacionAplicada());
    }
    RGBApixel colorBorde;
    //BORDE SUPERIOR
    colorBorde = this->colorBorde(colorCasillero, borde, Borde::SUPERIOR);
    for (int i = x*tamanioCelda; i<(x+1)*tamanioCelda; i++)
        for (int j = y*tamanioCelda; j<y*tamanioCelda+tamanioBorde; j++)
            grafico.SetPixel(i, j, colorBorde);
    //BORDE DERECHO
    colorBorde = this->colorBorde(colorCasillero, borde, Borde::DERECHO);
    for (int i = (x+1)*tamanioCelda-tamanioBorde; i<(x+1)*tamanioCelda; i++)
        for (int j = y*tamanioCelda; j<(y+1)*tamanioCelda; j++)
            grafico.SetPixel(i, j, colorBorde);
    //BORDE INFERIOR
    colorBorde = this->colorBorde(colorCasillero, borde, Borde::INFERIOR);
    for (int i = x*tamanioCelda; i<(x+1)*tamanioCelda; i++)
        for (int j = (y+1)*tamanioCelda-tamanioBorde; j<(y+1)*tamanioCelda; j++)
            grafico.SetPixel(i, j, colorBorde);
    //BORDE IZQUIERDO
    colorBorde = this->colorBorde(colorCasillero, borde, Borde::IZQUIERDO);
    for (int i = x*tamanioCelda; i<x*tamanioCelda+tamanioBorde; i++)
        for (int j = y*tamanioCelda; j<(y+1)*tamanioCelda; j++)
            grafico.SetPixel(i, j, colorBorde);
    //INTERIOR DEL CASILLERO
    for (int i = x*tamanioCelda+tamanioBorde; i < (x+1)*tamanioCelda-tamanioBorde; i++)
        for (int j = y*tamanioCelda+tamanioBorde; j < (y+1)*tamanioCelda-tamanioBorde; j++)
            grafico.SetPixel(i, j, colorCasillero);
}


RGBApixel Graficador::colorBorde(const RGBApixel& colorCasillero, const Borde& borde, Borde::Lado lado){
    if (borde.hayBorde(lado)){
        return this->paletaColores['1'];
    }else{
        return colorCasillero;
    }
}


void Graficador::graficarCuadricula(){
    for (int i = 0; i*tamanioCelda < tamanioCelda*cantCeldasDeAncho; i++){  //pinto lineas verticales de la cuadricula
        for (int j = 0; j < tamanioCelda*cantCeldasDeAlto; j++){
            this-> grafico.SetPixel(i*tamanioCelda, j, paletaColores['1']);
        }
    }
    for (int j = 0; j*tamanioCelda < tamanioCelda*cantCeldasDeAlto; j++){  //pinto lineas horizontales de la cuadricula
        for (int i = 0; i < tamanioCelda*cantCeldasDeAncho; i++){
            this-> grafico.SetPixel(i, j*tamanioCelda, paletaColores['1']);
        }
    }
}


void Graficador::exportarImagen(std::string ruta){
    grafico.WriteToFile(ruta.c_str());
}


Graficador::~Graficador(){
    //dtor
}
