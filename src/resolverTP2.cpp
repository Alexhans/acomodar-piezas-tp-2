#include "encastrador.h"


void Encastrador::resolverTP2(){

    tablero.vaciarTablero();


    Catalogo::CatalogoUnicoMascaras catalogoMascaras = catalogo.getCatalogo();
    std::vector<char> secuenciaOrdenada = ordenarSecuenciaEnBaseACatalogo(catalogoMascaras, secuencia.getSecuencia());

    // Ahora que ya tenemos el orden deseado.  Empezar a intentar la insercion de piezas.

    const int ALTO_TABLERO = tablero.obtenerAlto();
    const int ANCHO_TABLERO = tablero.obtenerAncho();

    int longitudSecuencia = secuencia.getCantidadDeElementos();

    // Matriz preparada para ciclar por las transformaciones
    const int CANTIDAD_DE_TRANSFORMACIONES = 6;
    Transformacion::Tipo tipoTrans[CANTIDAD_DE_TRANSFORMACIONES] = {
        Transformacion::ARRIBA,
        Transformacion::DERECHA,
        Transformacion::ABAJO,
        Transformacion::IZQUIERDA,
        Transformacion::ESPEJO_VERTICAL,
        Transformacion::ESPEJO_HORIZONTAL
    };
    short int contadorTrans = 0;  // usado para iterar por las transformaciones.


    for(int seqNum = 0; seqNum < longitudSecuencia; ++seqNum) {

        char nombrePiezaAIngresar = secuenciaOrdenada[seqNum];

        Pieza *piezaAIngresar = &catalogo.getPieza(nombrePiezaAIngresar)->second;

        const int ALTO_PIEZA = piezaAIngresar->getAlto();
        const int ANCHO_PIEZA = piezaAIngresar->getAncho();

        // Clase que realizar� las transformaciones.  (comienza con arriba)
        Transformacion transformacion(Transformacion::ARRIBA);

        bool esPosibleAgregar = false;
        int x = 0, y = 0;

        for(y = 0; !esPosibleAgregar && y < ALTO_TABLERO; ++y) {
            for(x = 0; !esPosibleAgregar && x < ANCHO_TABLERO; ++x) {

                esPosibleAgregar = false;  // Por defecto asumir que no esPosibleAgregar

                contadorTrans = 0;

                // Prueba las diferentes transformaciones hasta que alguna no colisione (o sea
                // que puede ingresarla en la grilla) o se terminen las transformaciones.
                while(!esPosibleAgregar && contadorTrans < CANTIDAD_DE_TRANSFORMACIONES) {

                    transformacion.setTipo(tipoTrans[contadorTrans]);
                    bool hagoElEsPosibleAgregar = fijarseSiEntra(piezaAIngresar, tablero.obtenerCasillero(x,y).obtenerPieza(), ALTO_PIEZA, ANCHO_PIEZA, contadorTrans);

                    if( hagoElEsPosibleAgregar ) {
                        // verifica si esPosibleAgregar
                        esPosibleAgregar = tablero.esPosibleAgregar(piezaAIngresar, x, y, transformacion.getTipo());
                    }
                    contadorTrans++;
                }
            }
        }
        // si es posible agregarla, lo hace.
        if(esPosibleAgregar) {
            x--; y--;   //HACK porque el for hace un aumento de mas al evaluar la condicion
            tablero.agregar(piezaAIngresar, x, y, transformacion.getTipo());
        }
    }
}


/* OPTIMIZACION:
 * si el ancla de la transformacion esta ocupada y y la posicion en la cual la quiere ingresar tambien lo esta, no verifica nada mas,
 * pasa a la siguiente transformacion.
 * Para cada transformacion checkea si el casillero que se transforma en el superior izquierdo
 * de la pieza a ingresar (ancla) y el del tablero est�n ocupados.
*/
bool Encastrador::fijarseSiEntra(Pieza* piezaAIngresar, bool grillaOcupada, const int ALTO_PIEZA, const int ANCHO_PIEZA,
                                 short int contadorTrans) const{

    bool anclaYGrillaOcupadas;  // se fija si el punto superior izq de la pieza a insertar est�

    switch (contadorTrans){
        case 0:
            anclaYGrillaOcupadas = grillaOcupada && piezaAIngresar->estaOcupado(0,0);
            break;
        case 1:
            anclaYGrillaOcupadas = grillaOcupada && piezaAIngresar->estaOcupado(0,ALTO_PIEZA-1);
            break;
        case 2:
            anclaYGrillaOcupadas = grillaOcupada && piezaAIngresar->estaOcupado(ANCHO_PIEZA-1, ALTO_PIEZA-1);
            break;
        case 3:
            anclaYGrillaOcupadas = grillaOcupada && piezaAIngresar->estaOcupado(ANCHO_PIEZA-1,0);
            break;
        case 4:
            anclaYGrillaOcupadas = grillaOcupada && piezaAIngresar->estaOcupado(0,ALTO_PIEZA-1);
            break;
        case 5:
            anclaYGrillaOcupadas = grillaOcupada && piezaAIngresar->estaOcupado(ANCHO_PIEZA-1,0);
            break;
        default:
            anclaYGrillaOcupadas = grillaOcupada && piezaAIngresar->estaOcupado(0,0);
            break;
    }
    return (!anclaYGrillaOcupadas);
}
