#include "secuencia.h"
#include <cstdio>
#include <cstdlib>

Secuencia::Secuencia(const std::string &ruta) {
    cargarSecuencia(ruta);
}

Secuencia::~Secuencia() {
    //dtor
}

void Secuencia::cargarSecuencia(const std::string &ruta) {
    // secuencia temporal para no modificar la original hasta no terminar la lectura
    std::vector<char> secuenciaTemporal;
    char elementoSecuencia;

    //cuenta cuantos elementos ingresaron en la secuencia

    // abrir archivo
    std::ifstream archivoSecuencia(ruta.c_str(), std::ifstream::in);
    if(!archivoSecuencia) { std::cerr << ruta.c_str() << " no pudo ser abierto" << std::endl; exit(1);}


    archivoSecuencia >> ancho;
    archivoSecuencia >> alto;

    while(!archivoSecuencia.eof()) {
        archivoSecuencia >> elementoSecuencia;
        secuenciaTemporal.push_back(elementoSecuencia);
    }

    // TODO: Fix this hack of duplicate last push_back
    secuenciaTemporal.pop_back();

    // cerrar archivo
    archivoSecuencia.close();

    // asignar nueva secuencia sobre la anterior.
    this->secuencia = secuenciaTemporal;
}

char Secuencia::getSiguienteDeSecuencia(int indice) {
    return this->secuencia[indice];
}

int Secuencia::getCantidadDeElementos() const {
    return secuencia.size();
}

int Secuencia::getAlto() const{
    return alto;
}

int Secuencia::getAncho() const{
    return ancho;
}

std::vector<char> Secuencia::getSecuencia() const{

    return this->secuencia;
}
