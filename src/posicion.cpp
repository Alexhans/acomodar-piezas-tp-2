#include "posicion.h"

///////////////////////////////////////////////////////////////////////////////
//  POSICION
///////////////////////////////////////////////////////////////////////////////
/*Posicion& Posicion::operator=(Posicion rhs) {
    x = rhs.getX();
    y = rhs.getY();
    return *this;
}*/

// TODO: Checkear que est�n bien y completar los operadores aritmeticos.

Posicion::Posicion(int x, int y) :x(x), y(y) {}
Posicion::Posicion(const Posicion& that): x(that.getX()), y(that.getY()) {}

// Posicion& operator+=(const Posicion& rhs) {
//     x += rhs.getX();
//     y += rhs.getY();
//     return *this;
// }

Posicion operator+(Posicion lhs, const Posicion& rhs) {
    /* lhs += rhs; */
    // lhs.x =
    return Posicion(lhs.getX() + rhs.getX(), lhs.getY() + rhs.getY());
}

Posicion operator-(Posicion lhs, const Posicion& rhs) {
    return Posicion(lhs.getX() - rhs.getX(), lhs.getY() - rhs.getY());
}


bool operator==(const Posicion& lhs, const Posicion& rhs) {
    return (lhs.x == rhs.x &&
           lhs.y == rhs.y);
}

bool operator!= (const Posicion& lhs, const Posicion& rhs) {
    return !operator==(lhs, rhs);
}

bool operator< (const Posicion& lhs, const Posicion& rhs) {
    if(lhs.y == rhs.y) return lhs.x < rhs.x;
    return lhs.y < rhs.y;
}
bool operator> (const Posicion& lhs, const Posicion& rhs) {
    return  operator<(rhs,lhs);
}

bool operator<=(const Posicion& lhs, const Posicion& rhs){
    return !operator> (lhs,rhs);
}

bool operator>=(const Posicion& lhs, const Posicion& rhs){
    return !operator< (lhs,rhs);
}

bool operator> (Posicion& lhs, Posicion& rhs){
    return operator<(lhs, rhs);
}

std::ostream& operator<<(std::ostream& os, const Posicion& obj) {
    os << "(" << obj.x << ", " <<obj.y << ")";
    return os;
}
