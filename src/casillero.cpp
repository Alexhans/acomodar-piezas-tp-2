#include "casillero.h"

Casillero::Casillero():
    coordXRel(-1),
    coordYRel(-1),
    piezaEnCasillero(0),
    transformacionAplicada(Transformacion::ARRIBA) {
}
void Casillero::agregarPieza(Pieza *pieza,
                  int coordx,
                  int coordy,
                  Transformacion::Tipo trans=Transformacion::ARRIBA) {
    piezaEnCasillero = pieza;
    coordXRel = coordx;
    coordYRel = coordy;
    transformacionAplicada = trans;
}

void Casillero::vaciar() {
    piezaEnCasillero = 0;
    coordXRel = -1;
    coordYRel = -1;
    transformacionAplicada = Transformacion::ARRIBA;
}

Casillero::~Casillero() {
    //dtor
}

Pieza* Casillero::obtenerPieza() {
    return piezaEnCasillero;
}

int Casillero::obtenerCoordenadaXRelativa() const {
        return coordXRel;
}

int Casillero::obtenerCoordenadaYRelativa() const {
        return coordYRel;
}

Transformacion::Tipo Casillero::getTransformacionAplicada() const {
    return this->transformacionAplicada;
}

