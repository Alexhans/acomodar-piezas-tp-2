///////////////////////////////////////////////////////////////////////////////
//  TRANSFORMACION
///////////////////////////////////////////////////////////////////////////////

#include "transformacion.h"

Transformacion::Transformacion(Tipo trans): transformacion(trans) {}

Transformacion::Tipo Transformacion::getTipo() const {return transformacion;}

void Transformacion::setTipo(Transformacion::Tipo trans) { transformacion = trans; }

Mascara Transformacion::transformar(const Mascara &original) {
    /* Con el switch me fijo que tipo de transformacion tengo que aplicar.
    */
    switch(this->getTipo()){
    case ARRIBA:
        return original;
    case ABAJO:
        return this->rotarAbajo(original);
    case DERECHA:
        return this->rotarDerecha(original);
    case IZQUIERDA:
        return this->rotarIzquierda(original);
    case ESPEJO_HORIZONTAL:
        return this->espejarHorizontal(original);
    case ESPEJO_VERTICAL:
        return this->espejarVertical(original);
    }
    return original;
}

/* Dependiendo de que tipo de transformacion tengo que aplicar puede ser que cuando se cree la nueva mascara, que va a contener la transformacion,
   se cambie el alto por el ancho, eso solo sucede cuando se aplica una rotacion de 90�, pero no cuando se aplica una de 180�.
   Se va recorriendo la masacaraOrigial y se obtiene su valor para ver si esta ocupado o no cada celda.
   Luego en otra mascara se rellena, mediante operaciones algebraicas, con la nueva mascara transformada.
*/

Mascara Transformacion::espejarVertical(const Mascara &original){
    Mascara mascaraFinal = Mascara(original.getAncho(), original.getAlto());
    for (int y = 0;y < original.getAlto(); y++){
        for(int x = 0; x < original.getAncho();x++){
            bool valor = original.getValor(x,y);
            int nuevoX = x;
            int nuevoY = original.getAlto() - (y + 1);
            mascaraFinal.setValor(nuevoX, nuevoY, valor);
        }
    }
    return mascaraFinal;
}

Mascara Transformacion::espejarHorizontal(const Mascara &original){
    Mascara mascaraFinal = Mascara(original.getAncho(), original.getAlto());
    for (int y = 0;y < original.getAlto();y++){
        for(int x = 0; x < original.getAncho(); x++){
            bool valor = original.getValor(x, y);
            int nuevoX = original.getAncho() - (x + 1);
            int nuevoY = y;
            mascaraFinal.setValor(nuevoX, nuevoY, valor);
        }
    }
    return mascaraFinal;
}

Mascara Transformacion::rotarDerecha(const Mascara &original){
     Mascara mascaraFinal = Mascara(original.getAlto(), original.getAncho());
             for(int y = 0;y < original.getAlto();y++){
                for(int x = 0;x < original.getAncho();x++){
                    bool valor = original.getValor(x,y);
                    int nuevoX =original.getAlto() - y - 1 ;
                    int nuevoY =x;
                    mascaraFinal.setValor(nuevoX, nuevoY, valor);
                }
             }
    return mascaraFinal;
}

Mascara Transformacion::rotarIzquierda(const Mascara &original){
     Mascara mascaraFinal = Mascara(original.getAlto(), original.getAncho());
             for(int y = 0;y < original.getAlto();y++){
                for(int x = 0;x < original.getAncho();x++){
                    bool valor = original.getValor(x,y);
                    int nuevoX =y;
                    int nuevoY =original.getAncho() - x - 1;
                    mascaraFinal.setValor(nuevoX, nuevoY, valor);
                }
             }
    return mascaraFinal;
}

Mascara Transformacion::rotarAbajo(const Mascara &original){
     Mascara mascaraFinal = Mascara(original.getAncho(), original.getAlto());
             for(int y = 0;y < original.getAlto();y++){
                for(int x = 0;x < original.getAncho();x++){
                    bool valor = original.getValor(x,y);
                    int nuevoX =original.getAncho() - x - 1 ;
                    int nuevoY =original.getAlto() - y - 1;
                    mascaraFinal.setValor(nuevoX, nuevoY, valor);
                }
             }
    return mascaraFinal;
}
