#include <iostream>
#include "encastrador.h"
#include "secuencia.h"
#include <algorithm>

int main()
{
    Catalogo primerCatalogo("default.txt");
    Secuencia primeraSecuencia("PruebaSecuencia.txt");

    std::vector<char> secuenciaOrdenada;
    std::vector<char> secuenciaAOrdenar=primeraSecuencia.getSecuencia();
    std::map<char, Pieza> catalogoOrdenada=primerCatalogo.getCatalogo();

    secuenciaOrdenada=ordenarSecuenciaEnBaseACatalogo(catalogoOrdenada,secuenciaAOrdenar);


    int cant = secuenciaOrdenada.size();
    int i = 0;
    char c;
    while (i<cant)
    {

        c = secuenciaOrdenada[i];
        std::cout << c << std::endl;
        i++;
    }

    return 0;
}
