#include <iostream>
#include "secuencia.h"

int main()
{
    Secuencia primeraSecuencia("PruebaSecuencia.txt");
    int cant = primeraSecuencia.getCantidadDeElementos();
    int i = 0;
    char c;
    while (i<cant)
    {

        c = primeraSecuencia.getSiguienteDeSecuencia(i);
        std::cout << c << std::endl;
        i++;
    }

    return 0;
}
