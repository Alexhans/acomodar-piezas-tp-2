#include <iostream>
#include <string>
#include <fstream>
#include "encastrador.h"

static int total_pruebas = 0;
static int pruebas_pasadas = 0;
class EncastSol : public Encastrador {
public:
    EncastSol(const std::string &a, const std::string &b):Encastrador(a,b) {} 
    Tablero& getTablero() { return tablero ;}
    Mascara obtenerMascara() { 
        Mascara total(tablero.obtenerAncho(), tablero.obtenerAlto());
        for(int y=0;y<total.getAlto();++y) {
            for(int x=0;x<total.getAncho();++x) {
                if(tablero.obtenerCasillero(x,y).obtenerPieza()) {
                    total.setValor(x, y, true);
                }
            }
        }
        return total;
    }
    Mascara obtenerPrimerPiezaDeCatalogo() {
        return catalogo.getPieza('A')->second.getMascara();
    }
    void mostrarInfoCatalogo() {
        catalogo.getCatalogo();
    //     mostrarCatalogo(catalogo.getCatalogo());
    }
};

bool verificarResolucion(std::string rutaCatalogo,
                        std::string rutaSecuencia,
                        std::string nombreMetodo,
                        std::string rutaEsperado) {
    using namespace std;
    cout << rutaCatalogo << endl;
    cout << rutaSecuencia << endl;
    cout << nombreMetodo << endl;
    cout << rutaEsperado << endl;
    EncastSol encastrador(rutaSecuencia, rutaCatalogo);
    EncastSol esperadoEnc(rutaSecuencia, rutaEsperado);
    esperadoEnc.mostrarInfoDatosCargados();
    // CatalogoUnicoMascaras catConMascaraEsperada;
    // cargarCatalogo(&catConMascaraEsperada, rutaEsperado);

    if(nombreMetodo == "TP1") {
        std::cout << "intentando solucion TP1";
        encastrador.resolver(EncastSol::TP1);
    }
    else if(nombreMetodo == "panch") { 
       std::cout << "intentando solucion panc";
       encastrador.resolver(EncastSol::Panch);
    }
    else { std::cout << "metodo inexistente\n"; return false; }

    bool solucionesSonIguales = false;
    // Mascara mascaraFinal = solucion.getMascaraFinal();
    // Mascara mascaraEsperada = catConMascaraEsperada['A'];
    // solucionesSonIguales = (mascaraFinal == mascaraEsperada);
    
    Mascara mascaraFinal = encastrador.obtenerMascara();
    Mascara mascaraEsperada = esperadoEnc.obtenerPrimerPiezaDeCatalogo();
    solucionesSonIguales = (mascaraFinal == mascaraEsperada);
    std::cout << "\nEsperada\n\n";
    mascaraEsperada.show(false);
    std::cout << "\nObtenida\n\n";
    mascaraFinal.show(false);

    std::cout << (solucionesSonIguales? "SUCCESS" : "**FAILURE**") << std::endl;
    total_pruebas++;
    if(solucionesSonIguales) pruebas_pasadas++;
    // mostrarInfoSolucion(solucion);
    encastrador.setRutaImagen(rutaSecuencia+nombreMetodo+".bmp");
    encastrador.representar();

    return solucionesSonIguales;

}
int main(int argc, char** argv) {
    using namespace std;
    string archivoPruebas = "pruebastp2.txt";
    if(argc > 1) { archivoPruebas = argv[1]; }

    ifstream archivo(archivoPruebas.c_str(), ifstream::in);
    string rutaCatalogo, rutaSecuencia, nombreMetodo, rutaEsperado;
    const int CANT_LECTURAS = 4;
    string* cosasParaLeer[CANT_LECTURAS] = {&rutaCatalogo, &rutaSecuencia, &nombreMetodo, &rutaEsperado};
    int i = 0;
    while(!archivo.eof()) {
        // sin checkeo de errores en caso de que no sean de a 4
        safeGetline(archivo, *cosasParaLeer[i]);
        i++;
        i = i % 4;

        if(i == 0) {
            verificarResolucion(rutaCatalogo, rutaSecuencia, nombreMetodo, rutaEsperado);
        }

    }

    std::cout << "\npruebas pasadas: " << pruebas_pasadas << "/" << total_pruebas << std::endl;
    archivo.close();
}


