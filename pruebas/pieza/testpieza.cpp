#include <iostream>
#include "mascara.h"
#include "catalogo.h"
#include "pieza.h"

int main() {
    using namespace std;
    Mascara mascara;
    Catalogo::CatalogoUnicoMascaras cat;
    Catalogo catalogo("../../../data/catalogo/default.txt");
    cat = catalogo.getCatalogo();

    for(Catalogo::CatalogoUnicoMascaras::iterator it=cat.begin();
        it!=cat.end(); ++it) {
        Pieza pieza(it->second);
        cout << "pieza\nnombre: "<< pieza.getNombre() << endl;
        pieza.debug_mostrar();

        // pieza.estaOcupado(0,0);
        cout << "alto: " << pieza.getAlto() << endl;
        cout << "ancho: " << pieza.getAncho() << endl;
        pieza.getBorde(0,0);
        cout << "nombre: " << pieza.getNombre() << endl;
    }


}
