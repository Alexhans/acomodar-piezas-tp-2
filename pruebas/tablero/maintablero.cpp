#include <iostream>
#include "tablero.h"
#include "secuencia.h"
#include "catalogo.h"

//TODO:
// agregar
// esPosibleAgregar

int main() {
    using namespace std;
    Secuencia secuencia("secuencia_prueba.txt");
    Tablero tab(secuencia.getAncho(), secuencia.getAlto());
    Catalogo catalogo("../../data/catalogo/default.txt");
    // tab.cambiarDimension(5, 5);
    int w = tab.obtenerAncho();
    int h = tab.obtenerAlto();
    cout << (w == 10) << " " << (h == 5) << endl;
    cout << h << " " << w << endl;
    mostrarTablero(tab);
    int lugOcupados = tab.obtenerCantidadDeLugaresOcupados();
    cout << "lugOcupados: " << lugOcupados << endl;
    Pieza *pieza = catalogo.getPieza('C');
    tab.agregar(pieza, 1, 2); 

    pieza = catalogo.getPieza('A');
    tab.agregar(pieza, 4, 4); 

    cout << (tab.obtenerCasillero(1,2).obtenerPieza()?'1':'0') << endl;
    cout << (tab.obtenerCasillero(0,0).obtenerPieza()?'1':'0') << endl;

    lugOcupados = tab.obtenerCantidadDeLugaresOcupados();
    cout << "lugOcupados: " << lugOcupados << endl;

}
