                     Programa Acomodar

Toma un catalogo de piezas y una secuencia de las mismas y lo acomoda en un tablero espeficado de una manera
tratando de compactar todo lo posible de una manera eficaz. 

Realiza este analisis con el algoritmo del TP 1 y el del TP 2 (propio) y muestra estadísticas de ocupación y uso de piezas.

Finalmente, representa en un archivo de imagen la solución del algoritmo del TP2.

Instrucciones de uso:

Uso: acomodar.exe [ruta_secuencias] [ruta_catalogo]

Para el uso del programa precisará de por lo menos 2 archivos:
Uno con el catálogo de piezas posibles y otro con la secuencia de piezas a ingresar.

El formato en el archivo de catalogo es el siguiente: Hay que ingresar un valor n,
el cual indica cuantas filas se van a leer, este valor n tiene que ser igual a la altura de la
pieza. Las x representan lugares ocupados, las cuales forman la pieza. Ejemplo:

3
xxx
 x
 x
2
xx
xx

Por cada pieza que lee satisfactoriamente, le asigna una letra secuencialmente, siendo la 'A' (A mayúscula) la primera, 
'B' la segunda, etc. 


El formato en el archivo de la secuencia es la siguiente: Se tienen que ingresar dos numeros,
uno en cada linea, donde el primer numero equivale a la cantidad de filas del tablero
donde se van a colocar las fichas y el segundo numero equivale a la cantidad de columnas.
Luego separadas por ENTERS se colocan las letras correspondientes a las fichas que se desean
colocarn en el tablero. Ejemplo:

50
35
K
K
I
H
F
E
E



Versiones por defecto de estos archivos se encuentran en las rutas
/data/catalogo/ 
 y  
/data/secuencias/
respectivamente. 

El programa leerá las piezas que se ingresen en la secuencia y creará una imagen BMP, en la misma
carpeta donde se encuentra el proyecto con el nombre resolucion.bmp, donde se podrá ver el gráfico resultante de la
combinación de las piezas.
