#ifndef GRAFICADOR_H
#define GRAFICADOR_H

#include <map>
#include <string>
#include "tablero.h"
#include "casillero.h"
#include "pieza.h"
#include "borde.h"
#include "EasyBMP.h"

/* Se ocupa de representar el tablero export�ndolo a imagenes en un archivo.
*/
class Graficador
{
    public:

        /* Post: Graficador creado con un grafico de BitDepth = 32 y celdas de un tama�o igual a la constante TAMANIO_CELDA.
            Paleta de colores inicializada por defecto (blanco, negro y otros 14 colores).
        */
        Graficador();

        /* Post: Queda modificado el atributo 'grafico' como representacion del tablero (lleno) pasado por parametro.
        */
        void graficar(Tablero* tablero);

        /* Pre: Tablero graficado en el atributo 'grafico' (mediante el metodo 'graficar').
            Post: Queda guardado el grafico en un archivo .bmp en la ruta pasada por parametro.
        */
        void exportarImagen(std::string ruta);

        virtual ~Graficador();

        static int TAMANIO_CELDA; //tama�o default (en pixels) del lado de las celdas del tablero

    private:
        void inicializarPaletaColores();
        void graficarCasillero(Tablero* tablero, int x, int y);
        void graficarCuadricula();
        RGBApixel colorBorde(const RGBApixel& colorCasillero, const Borde& borde, Borde::Lado lado);

        BMP grafico;
        int cantCeldasDeAncho;
        int cantCeldasDeAlto;
        int tamanioCelda;   //tama�o (en pixels) del lado de las celdas del tablero
        std::map <char, RGBApixel> paletaColores;   //mapa <TipoDePieza, Color>
};

#endif // GRAFICADOR_H
