#ifndef TRANSFORMACION_H
#define TRANSFORMACION_H

#include "mascara.h"
///////////////////////////////////////////////////////////////////////////////
//  TRANSFORMACION
///////////////////////////////////////////////////////////////////////////////

    /* Se encarga de realizar las transformaciones a las piezas, las cuales estan enumeradas
     * en el enum Transformacion::Tipo;
     */

    class Transformacion {
    public:
        enum Tipo {
            ARRIBA,
            ABAJO,
            DERECHA,
            IZQUIERDA,
            ESPEJO_HORIZONTAL,
            ESPEJO_VERTICAL
        };

        /*Pre:
         *Pos: Crea la transformacion con el tipo pasado por argumento.
        */

        Transformacion(Tipo trans);

        /*Pre:
         *Pos: Devuelve el valor de la transformacion actual
        */
        Tipo getTipo() const;

        /* Post: setea el tipo de transformacion deseada
        */
        void setTipo(Tipo trans);

        /*
        *Pre: Mascara creada
        *Post: Devuelve una copia de la pieza ingresada, con la transformacion aplicada.
        */
        Mascara transformar(const Mascara &original);

    private:
        Tipo transformacion;
        Mascara rotarDerecha(const Mascara &original);
        Mascara rotarAbajo(const Mascara &original);
        Mascara rotarIzquierda(const Mascara &original);
        Mascara espejarHorizontal(const Mascara &original);
        Mascara espejarVertical(const Mascara &original);
    };

#endif
