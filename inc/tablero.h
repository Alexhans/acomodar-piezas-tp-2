#ifndef TABLERO_H
#define TABLERO_H

#include "casillero.h"
#include "pieza.h"
#include "transformacion.h"
#include "lista.h"

/*	Representa el tablero en el que se colocan las piezas.  */
class Tablero
{
    public:
        /* Post: Tablero creado con dimensiones nulas.
        */
        Tablero();

        /* Pre: ancho y alto mayores a 0.
        Post: Tablero creado con las dimensiones pasadas por parametro
        */
        Tablero(const int ancho, const int alto);

        virtual ~Tablero();

        /* Pre: (x,y) no se puede ir del rango de las dimensiones del tablero.
        Post: Queda agregada al tablero la pieza pasada, con la transformacion pasada, en la posicion pasada (x,y) del tablero.
            (La posicion representa el casillero a partir del cual se inserta la pieza, es decir, la posicion en la que
             se agrega la esquina superior izquierda de la pieza transformada)
        */
        void agregar(Pieza* pieza,int x,int y, Transformacion::Tipo transformacion = Transformacion::ARRIBA);

        /* Pre: (x,y) dentro de las dimensiones del tablero.
        Post: Devuelve si es posible agregar la pieza (transformada) en la posicion (x,y), es decir, si no se superpone
            con ninguna otra pieza ya insertada al querer agregar la nueva pieza ahi.
        */
        bool esPosibleAgregar(Pieza* pieza,
                              int x,
                              int y,
                              Transformacion::Tipo transformacion = Transformacion::ARRIBA); // aca iria un const pero me tira error con el obtenerCasillero

        /* Pre: (x,y) dentro de las dimensiones del tablero.
            Post: Devuelve una referencia al casillero en la posicion (x,y) del tablero.
        */
        Casillero& obtenerCasillero(int x, int y);

        int obtenerAlto() const;

        int obtenerAncho() ;

        /* devolver la cantidad de casilleros en los que hay
         * una pieza (no vacíos).*/
        int obtenerCantidadDeLugaresOcupados();

        int obtenerCantidadDePiezasAgregadas() const;

        /* Pre: alto y ancho mayores a 0, tablero previamente vacio.
            Post: Tablero redimensionado.
        */
        void cambiarDimension(int ancho, int alto);

        /* Post: Tablero vaciado, es decir, masntiene sus dimensiones con todos sus casilleros apuntando a NULL y
            coordenadas relativas (-1,-1).
        */
        void vaciarTablero();


        Lista< Lista <Casillero > > listaCasillero;

    private:

        int cantidadPiezasAgregadas;
};

/* Metodo no miembro que imprime por pantalla el tablero, con un 1 por cada casillero lleno y un 0 por cada uno vacio.*/
void mostrarTablero(Tablero& tablero);

#endif // TABLERO_H
