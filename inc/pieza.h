#ifndef PIEZA_H
#define PIEZA_H
#include "mascara.h"
#include "borde.h"
#include "transformacion.h"


class Pieza
{
    public:
        Pieza(char nombre, const Mascara &mascara);

        virtual ~Pieza();

        /* Pre: (x,y) no deben estar fuera del rango de la mascara de la pieza.
            Post: Devuelve si en la pieza ocupa o no la posicion (x,y) de su mascara.
        */
        bool estaOcupado(int x, int y) const;

        int getAlto() const;

        int getAncho() const;

        char getNombre() const;

        /* Pre: (x,y) no deben estar fuera del rango de la mascara de la pieza con la
            transformacion pasada por parametro.
           Post: Devuelve el objeto Borde en la posicion (x,y) de la pieza transformada.
        */
        Borde getBorde(int x, int y, Transformacion::Tipo transformacionAplicada);

        /* Post: Devuelve la mascara de la pieza sin tranformar.
        */
        Mascara& getMascara();

        /* Imprime por pantalla la mascara de la pieza sin transformar, con un 1 por cada casillero ocupado
        y un 0 por cada uno libre, junto con los subindices de las filas y columnas de las mascaras.*/
        void debug_mostrar();

    private:

        /* Calcula si en cada posicion (x,y) de la mascara, sus bordes(INFERIOR, SUPERIOR, DERECHO, IZQUIERDO) se encuentran ocupados
        o no. Llena una matriz con dichos datos.*/
        void calcularBordes();

        // Este getborde sirve para obtener el borde de la mascara de la nueva pieza ya transformada
        Borde getBorde(int x, int y);

        char nombre;
        Mascara mascara;
        std::vector <std::vector <Borde> > borde;  // Matriz de bordes.
        // encarar esto
};

#endif // PIEZA_H
