#ifndef CATALOGO_H
#define CATALOGO_H
#include <map> 
#include <string>

#include "pieza.h"

/* representa el cat�logo de piezas que obtiene de un archivo de texto 
con un formato que informa la altura y la forma:
ejemplo (pieza de tetris conocida):
2
 x
xxx
*/

class Catalogo
{
    public:
        typedef std::map<char, Pieza> CatalogoUnicoMascaras;

        /* Pre:: ruta de archivo existente, donde esta contenido el catalogo.
        Post: Catalogo creado y cargado a partir del archivo con la ruta pasada por parametro
        */
        Catalogo(const std::string &ruta);

        virtual ~Catalogo();

		/* Pre: Nombre de pieza existente.
            Post: Devuelve un puntero a la pieza, de la cual se le paso el nombre por paramtero.
		*/
		Catalogo::CatalogoUnicoMascaras::iterator getPieza(char nombrePieza);

        // TODO eventualmente, quitar el parametro (*catalogo que recibe y usar directamente el atributo
		void cargarCatalogo(Catalogo::CatalogoUnicoMascaras *catalogo, std::string archivo);

        /* Post: Devuelve el catalogo de piezas.
        */
        Catalogo::CatalogoUnicoMascaras getCatalogo() const;

    private:

        Catalogo::CatalogoUnicoMascaras catalogo;
};

void mostrarCatalogo(const Catalogo &cat);  //en funcionesmostrar.cpp

#endif // CATALOGO_H
