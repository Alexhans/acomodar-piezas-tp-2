#ifndef MASCARA_H
#define MASCARA_H
#include <string>
#include <vector>
#include <fstream>
#include <map>

#include "posicion.h"

    class Mascara;
    // version especializada para optimizar espacio.
    typedef std::vector<bool> SetDeBitsDinamico;
    typedef std::vector<SetDeBitsDinamico> MatrizDeBitsDinamica;

///////////////////////////////////////////////////////////////////////////////
//  MASCARA
///////////////////////////////////////////////////////////////////////////////

    /* Representa una matriz bidimensional con valores binarios. Representa que lugares de la pieza estan ocupados y cuales libres.
     */

    class Mascara {

    public:
        /*
        *  Pos: Mascara creada de dimensiones 'ancho' por 'alto' con valores false.
        */
        Mascara(unsigned int ancho=8, unsigned int alto=20);
        /*
          Pos: Crea la matriz dinamica con los datos de 'matrix'.
        */
        Mascara(const MatrizDeBitsDinamica &matrix);
        Mascara(const Mascara& that); //Constructor de copia
        Mascara& operator=(const Mascara& that); // Asignacion

        ~Mascara();

        friend bool operator== (const Mascara& lhs, const Mascara& rhs);
        friend bool operator!= (const Mascara& lhs, const Mascara& rhs);


        /* Post: muestra los datos de la mascara por pantalla.
           'coordenadas' controla si imprime coordenadas de ayuda.
        */
        void show(bool coordenadas=true) const;  // TODO sacar a funcion no miembro o sobrecargar <<.
                                                    //en funcionesmostrardebug.cpp

        /* Pos: Devuelve la altura de la Mascara(la cantidad de filas)  */
        int getAlto() const;

         /* Pos: Devuelve el ancho de la Mascara(la cantidad de columnas) */
        int getAncho() const;

        /* Pre: Se le pasan las coordenadas del casillero de la mascara del cual se desea conocer el valor
         * Pos: Devuelve un 1 si el casillero esta ocupado, devuelve un 0 si el casillero NO esta ocupado.
         */
        bool getValor(const int x, const int y) const ;

        /* Pre: Se le pasan las coordenadas del casillero donde se desea setear el valor.
         * Pos: Se le setea un 1 o un 0 dependiendo si se desea que el casillero este ocupado o no.
         */
        void setValor(const int x, const int y, bool valor);

    private:

        MatrizDeBitsDinamica data;

        // No lo hacemos publico porque el orden que requiere [y][x] no
        // es intuitivo.  Usar getValor(x,y).
        SetDeBitsDinamico& operator[](int indice);

    };




///////////////////////////////////////////////////////////////////////////////
//  NON MEMBER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////


    /*
    * Pre: Se le pasa un archivo con todo el catalogo de las piezas con un cierto formato. No se pueden ingresar mas de 190 distintos tipos de piezas.
    * Pos: Puebla un catalogo con las piezas y a cada pieza le asigna un valor a partir de la 'A'.
    */
    // void cargarCatalogo(CatalogoUnicoMascaras *catalogo, std::string archivo);

    /*Se encarga de darle forma rectangular a la pieza completando los espacios vac? con ceros
     *Pre:
     *Pos: Modifica la matriz completando el rectangulo de la mascara con false donde el size del vector no se condice con el maximo existente.

     ej:

     11          110
     111   ->    111
     1           100

    */
    void completarRectangulo(MatrizDeBitsDinamica &matrix);

    /*
    * Pre:
    * Pos: Devuelve cuantos casilleros de la Mascara estan ocupados por un 1
    */
    int contarSi(const Mascara& mascara, const bool valor=true);

    /* getline seguro para lecturas multiplataforma.
      había problemas con los EOL de Linux/Windows.  LF / CRLF

      pos: ver std::getline.
    */
    std::istream& safeGetline(std::istream& is, std::string& t);


#endif

