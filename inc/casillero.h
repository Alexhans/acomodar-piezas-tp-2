#ifndef CASILLERO_H
#define CASILLERO_H
#include "pieza.h"
#include "transformacion.h"

/* Representa el un casillero donde se puede ingresar una pieza. (parte de una, si la 
pieza es de una dimensión mayor a 1 x 1. */
class Casillero
{
    public:
        /* Post: Casillero creado apuntando a NULL(indica que no tiene ninguna pieza) con coordenadas relativas -1 y transformacion ARRIBA.
        */
        Casillero();

        virtual ~Casillero();

        /* Post: Devuelve un puntero a la pieza que ocupa el casillero. Si esta vacio, devuelve NULL.
        */
        Pieza* obtenerPieza();

        /* Post: Devuelve la posicion en X relativa a la pieza (transformada) que ocupa el casillero.
        */
        int obtenerCoordenadaXRelativa() const;

        /* Post: Devuelve la posicion en Y relativa a la pieza (transformada) que ocupa el casillero.
        */
        int obtenerCoordenadaYRelativa() const;

        //Ocupa el casillero con la parte de la pieza (transformada) indicada con los parametros.
        /* Post: piezaEnCasillero apunta a la pieza por parametro, modificados los
            valores de coordXRel, coordYRel, transformacionAplicada*/
        void agregarPieza(Pieza *pieza,
                          int coordx,
                          int coordy,
                          Transformacion::Tipo trans);

        /* Post: Casillero vaciado. Queda como despues de ser construido por defecto: apunta a NULL con coordenadas
            relativas -1 y transformacion ARRIBA.
        */
        void vaciar();

        /* Post: Devuelve el tipo de transformacion que tiene aplicada la pieza en ese casillero.
                Si el casillero se encuentra vacio(no hay pieza) devuelve por defecto ARRIBA.
        */
        Transformacion::Tipo getTransformacionAplicada() const;

        void debug_mostrar() const; //en funcionesmostrar.cpp

    private:
        int coordXRel;
        int coordYRel;
        Pieza* piezaEnCasillero;
        Transformacion::Tipo transformacionAplicada;
};

#endif // CASILLERO_H
