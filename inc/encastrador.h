#ifndef ENCASTRADOR_H
#define ENCASTRADOR_H
#include <string>

#include "graficador.h"
#include "catalogo.h"
#include "secuencia.h"
#include "tablero.h"

/* Clase principal para resolver el TP2.  Aglomera los métodos necesarios para el cargado, la resolución 
y la representación de los datos.*/
class Encastrador
{
    public:
        enum tipoResolucion { TP1, Panch };

        /* Pre: sec y cat (secuencia y catalogo) deben ser rutas de archivos existentes.
            Post: Crea e inicializa el catalogo y la secuencia a partir de los archivos de ruta cat y sec,
            crea un tablero vacio de las dimensiones dispuestas por el archivo de secuencia y crea el graficador por defecto.
        */
        Encastrador(std::string sec, std::string cat);

        virtual ~Encastrador();

        /* Post: Queda cambiada la ruta del archivo en el que se gurdara la imagen del tablero lleno.
        */
        void setRutaImagen(std::string rutaImagen);

		/* Post: Queda cargado el tablero con las piezas (segun el catalogo y la secuencia), insertandolas con el algoritmo
		del TP1 (insercion lineal) o del TP2 (Panch, con transformaciones)
		*/
		void resolver(Encastrador::tipoResolucion algoritmo);

		/* Post: Queda graficado el tablero (lleno) en el archivo de ruta rutaImagen.
		*/
		void graficar();

        /* Imprime por pantalla el mensaje de bienvenida */
        void mostrarMensajeBienvenida();

        /* Imprime por pantalla la secuencia cargada, las dimensiones del
        tablero dispuesto por esta y el total de piezas de la misma */
        void mostrarInfoDatosCargados();

        /* Grafica y muestra información acerca de la ocupación del tablero */
        void representar();

        /* Muestra informacion acerca de la ocupación del tablero, la cantidad de piezas usadas y la cantidad sin usar */
        void mostrarOcupacionYPiezasUsadas();

    protected:
        /* Encastra la piezas con el metodo de resolucion del TP1.
        */
        void resolverTP1();

        /* Encastra las piezas con el metodo de resolucion del TP2.
        */
        void resolverTP2();

        /* Controla si el ancla esta ocupado en la pieza y el tablero.
         * Devuelve si hay que fijarse o no si entra una pieza para el algoritmo del tp2. Sirve para separar el codigo.
         */
        bool fijarseSiEntra(Pieza* piezaAIngresar, bool grillaOcupada, const int ALTO_PIEZA, const int ANCHO_PIEZA,
                            short int contadorTrans) const;

        std::string archivoCatalogo;
        std::string archivoSecuencia;
        Catalogo catalogo;
        Secuencia secuencia;
        Tablero tablero;
        Graficador graficador;
        std::string rutaImagen;
        void mostrarSecuencia(const Secuencia& secuencia);
        void mostrarDimensiones(Tablero tablero);
};

/////////////////////////////////
//// NON MEMBER FUNCTIONS
/////////////////////////////////


typedef std::pair<char, Pieza> ParElementoPieza;
typedef std::map<char, Pieza> CatalogoUnicoPieza;

/* Post: Devuelve una copia de la secuencia original, pero ordenada de mayor a menor en base a la dimension de las piezas
    (y en caso de igual dimension, por altura u orden en el catalogo). La dimension de la pieza es igual al alto por el ancho de la pieza.
*/
std::vector<char> ordenarSecuenciaEnBaseACatalogo(const CatalogoUnicoPieza& catalogo, const std::vector<char> &secuencia);

#endif // ENCASTRADOR_H
