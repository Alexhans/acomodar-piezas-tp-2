#ifndef BORDE_H
#define BORDE_H

/* Se ocupa de de representar la información de los bordes posibles de un rectángulo */
class Borde
{
    public:
        enum Lado{SUPERIOR, DERECHO, INFERIOR, IZQUIERDO};

        /* Post: Borde creado con todos los atributos en false.
        */
        Borde();

        virtual ~Borde();

        /* Post: Setea si hay borde o no en el lado pasado como primer parametro, con el valor pasado en el segundo parametro.
        */
        void setBorde(Borde::Lado lado, bool valor);

        /* Post: Devuelve si hay borde o no en el lado pedido.
        */
        bool hayBorde(Borde::Lado lado) const;

    private:
        bool esBorde[4];    //0: SUPERIOR, 1: DERECHO, 2: INFERIOR, 3: IZQUIERDO
};

#endif // BORDE_H
