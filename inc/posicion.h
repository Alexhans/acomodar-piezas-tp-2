#ifndef POSICION_H
#define POSICION_H
#include <iostream>

///////////////////////////////////////////////////////////////////////////////
//  POSICION
///////////////////////////////////////////////////////////////////////////////

    /* representa puntos en el espacio 2d.
     */

    class Posicion {
    public:
        // NOTA: es importante tener un constructor por defecto para la
        // utilización resize/vector(n)
        // TODO limpiar esto con Posicion() e implementarlo en source.
        Posicion(int x=-1, int y=-1);

        Posicion(const Posicion& that);

        //Posicion& operator=(Posicion rhs);

        int getX() const {return x;}

        int getY() const {return y;}

        //TODO: Completar lo que falta.

        // Posicion& operator+=(const Posicion& rhs);
        friend Posicion operator+(Posicion lhs, const Posicion& rhs);
        // Posicion& operator-=(const Posicion& rhs);
        friend Posicion operator-(Posicion lhs, const Posicion& rhs);
        // friend Posicion& operator-=(const Posicion& rhs);
        // Posicion& operator-(Posicion lhs, const Posicion& rhs);

        friend bool operator< (const Posicion& lhs, const Posicion& rhs);
        friend bool operator> (const Posicion& lhs, const Posicion& rhs);
        friend bool operator<=(const Posicion& lhs, const Posicion& rhs);
        friend bool operator>=(const Posicion& lhs, const Posicion& rhs);
        friend bool operator== (const Posicion& lhs, const Posicion& rhs);
        friend bool operator!= (const Posicion& lhs, const Posicion& rhs);

        friend std::ostream& operator<<(std::ostream& os, const Posicion& obj);

    private:
        int x;
        int y;
    };
#endif // POSICION_H
