#ifndef SECUENCIA_H
#define SECUENCIA_H
#include "pieza.h"

#include <string>
#include <vector>

/* Representa una secuencia de piezas indexadas con caracteres empezando por la 'a' minuscula. */
class Secuencia
{
    public:
        /* Pre: Hay que pasarle la ruta del archivo donde esta le secuencia.
         * Pos: Crea la secuencia a partir de la encontrada en el archivo.
         */
        Secuencia(const std::string &ruta);

        /* Post: Devuelve el alto con el cual se tiene que crear el tablero.
        */
        int getAlto() const;

        /* Post: Devuelve el ancho con el cual se tiene que crear el tablero.
        */
        int getAncho() const;

        virtual ~Secuencia();

        /* Pre: Hay que pasarle la ruta del archivo donde esta le secuencia.
         * Pos: Carga la secuencia a partir de la encontrada en el archivo.
         */
        void cargarSecuencia(const std::string &ruta);

        /* Pos: Obtiene el siguiente elemento a procesar de la secuencia
        */
        char getSiguienteDeSecuencia(int indice);

        /* Pos: Devuelve la cantidad de elementos adentro de la secuencia.
        */
        int getCantidadDeElementos() const;

        /* Pre: Secuencia ya cargada.
         * Post: Devuelve la secuencia entera.
         */
        std::vector<char> getSecuencia() const;

    private:

        std::vector<char> secuencia;
        int alto;
        int ancho;

};

#endif // SECUENCIA_H
